import unittest
from .corrstat import CorrStat, format_ac_data


class ConnectTestCase(unittest.TestCase):

    eis_params = {
        'freq_max_hz': 1,
        'freq_min_hz': 0.002,
        'points': 3,
        'amplitude_mv': 20,
        'ocp': CorrStat.OCP_STATIC,
        'sens': CorrStat.SCANRATE_NORMAL
    }

    def test_connect(self):
        # Test if the device is connected (check that the device is
        # phsysically connected)
        print("Test connect")
        cstat = CorrStat()
        self.assertTrue(cstat.is_connected)

    def test_is_ready_and_stop(self):
        print("Test if ready")
        cstat = CorrStat()
        self.assertTrue(cstat.is_ready)

        print("Test if busy")
        cstat.set_ac_config(self.eis_params)
        cstat.send_ac_config()
        g = cstat.read_ac_data()
        next(g)
        self.assertFalse(cstat.is_ready)
        self.assertTrue(cstat.is_busy)

        print("Test the stop action")
        if cstat.is_busy:
            cstat.stop()


class UidTestCase(unittest.TestCase):
    def test_get_uid(self):
        print("Test get UID")
        c = CorrStat()
        uid = c.get_uid()
        print("UID: ", uid)
        self.assertIn('MC', uid)
        number = int(uid[2:])
        self.assertGreater(number, 0)
        self.assertLess(number, 1000)


class RunExperimentTestCase(unittest.TestCase):

    eis_params = {
        'freq_max_hz': 1000,
        'freq_min_hz': 5,
        'points': 3,
        'amplitude_mv': 20,
        'ocp': CorrStat.OCP_STATIC,
        'sens': CorrStat.SCANRATE_NORMAL
    }

    def test_run_experiment(self):
        print('Configure experiment')

        cstat = CorrStat()
        cstat.stop()
        self.assertTrue(cstat.is_ready)
        print('Device Ready')

        cstat.set_ac_config(self.eis_params)
        cstat.send_ac_config()

        has_started = False
        for actual_point, data_ in enumerate(cstat.read_ac_data(format_ac_data)):
            print('Actual point: ', actual_point)
            has_started = True
        self.assertTrue(has_started)


class DCconfigTestCase(unittest.TestCase):
    dc_params_0 = {
        'initial_potential_mv': -20,
        'final_potential_mv': 20,
        'volt_ref': 'OCP',
        'points': 10,
        'scan_type': 'Lineal',
        'scanrate_uvps': 133
    }

    dc_params_1 = {
        'initial_potential_mv': -20,
        'final_potential_mv': 20,
        'volt_ref': 'Eref',
        'points': 10,
        'scan_type': 'Cíclica',
        'scanrate_uvps': 133
    }

    dc_params_2 = {
        'initial_potential_mv': 20,
        'final_potential_mv': -20,
        'volt_ref': 'OCP',
        'points': 10,
        'scan_type': 'Lineal',
        'scanrate_uvps': 133
    }

    dc_params_3 = {
        'initial_potential_mv': 20,
        'final_potential_mv': -20,
        'volt_ref': 'Eref',
        'points': 10,
        'scan_type': 'Cíclica',
        'scanrate_uvps': 133
    }

    def test_set_dc_config(self):
        cstat = CorrStat()
        cstat.stop()

        cstat.set_dc_config(self.dc_params_0)
        print(cstat.gen_dc_config)
        
        cstat.set_dc_config(self.dc_params_1)
        print(cstat.gen_dc_config)

        cstat.set_dc_config(self.dc_params_2)
        print(cstat.gen_dc_config)

        cstat.set_dc_config(self.dc_params_3)
        print(cstat.gen_dc_config)

    def test_send_dc_config(self):
        cstat = CorrStat()
        cstat.stop()
        cstat.set_dc_config(self.dc_params_0)
        cstat.send_config(cstat.gen_dc_config)

    def test_start_dc_scan(self):
        cstat = CorrStat()
        cstat.stop()
        cstat.set_dc_config(self.dc_params_0)
        cstat.send_config(cstat.gen_dc_config)
        for data in cstat.read_dc_data():
            print(data)



if __name__ == "__main__":
    unittest.main()
