import serial
import json
import math
import time
import struct
import collections
import sys
import glob

CORRSTAT_NAME = 'Cstat'
CORRSTAT_BAUDRATE = 9600


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        # ports = glob.glob('/dev/tty[A-Za-z]*')
        ports = glob.glob('/dev/ttyUSB[0-9]')
        # ports.extend(glob.glob('/dev/serial[0-9]'))
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def open_serial_port(port: str):
    # print('*** Abriendo puerto serial ***')
    try:
        comm = serial.Serial(
            port=port,
            baudrate=CORRSTAT_BAUDRATE,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=3
        )
        if comm.isOpen():
            print('Conectado al puerto: {}'.format(port))
            return comm
    except Exception as inst:
        print('No se pudo conectar al puerto: {}'.format(port))
        print(inst)
    return None


class CorrStatRegs:
    """
    CorrStatRegs:
    Objeto que representa los registros de control del
    potenciostato CorrStat
    """
    # COMMANDOS DE LA MÁQUINA
    CMD_IDENTIFY = '*idn'
    CMD_UID = '*uid'
    TAG_ID = CORRSTAT_NAME
    TAG_EOL = ';'
    # Comandos de AC
    CMD_AC_FHIGH_INT = '*fH'
    CMD_AC_FHIGH_DEC = '*fh'
    CMD_AC_FLOW_INT = '*fL'
    CMD_AC_FLOW_DEC = '*fl'
    CMD_AC_FPOINTS = '*fn'
    CMD_AC_AMP = '*as'
    CMD_AC_SCAN_RATE = '*b'
    CMD_AC_OCP_TYPE = '*o'
    CMD_AC_START = '*hf0'
    CMD_AC_STOP = 'Q'
    # ARGUMENTOS AC
    ARG_AC_OCP_DYNAMIC = '0'
    ARG_AC_OCP_STATIC = '1'
    ARG_AC_SCANRATE_SLOW = '0'
    ARG_AC_SCANRATE_MID = '1'
    ARG_AC_SCANRATE_FAST = '2'
    # etiquetas de identificación en los datos AC
    TAG_AC_OK_CURRENT = 'OKIC'
    TAG_AC_OK_VOLTAGE = 'OKOC'
    TAG_AC_OK_RANGE = 'OKFN'
    TAG_AC_OUT_OF_RANGE = 'ORNG'
    TAG_AC_STARTED = 'SHF'
    # Commandos de DC
    CMD_DC_TYPE = '*cyc'
    CMD_DC_ABS_LOWVOLT_POS = '*vlpa'
    CMD_DC_ABS_LOWVOLT_NEG = '*vlna'
    CMD_DC_ABS_HIGHVOLT_POS = '*vhpa'
    CMD_DC_ABS_HIGHVOLT_NEG = '*vhna'
    CMD_DC_REL_LOWVOLT_POS = '*vlpr'
    CMD_DC_REL_LOWVOLT_NEG = '*vlnr'
    CMD_DC_REL_HIGHVOLT_POS = '*vhpr'
    CMD_DC_REL_HIGHVOLT_NEG = '*vhnr'
    CMD_DC_SCANRATE = '*ssc'
    CMD_DC_NPOINTS = '*pts'
    CMD_DC_START = '*hf1'
    CDM_DC_STOP = '*FIN'
    CDM_DC_RESET = 'R'
    # ARGUMENTOS DC
    ARG_DC_LINEAR_POS = '0'
    ARG_DC_CYCLIC_POS = '1'
    ARG_DC_LINEAR_NEG = '2'
    ARG_DC_CYCLIC_NEG = '3'
    # etiquetas de identificación en los datos DC
    # TAG_DC_OUT_OF_RANGE = '*OUTRN'
    # TAG_DC_CURRENT_OUT_OF_RANGE = '*OUT;'
    # TAG_DC_OK_RANGE = '*OK_DCM'
    # TAG_OK_MEAS = '*ADC;'
    TAG_DC_START_SCAN = '*OKDA'
    TAG_DC_OUT_OF_RANGE = 'ORNG'
    TAG_DC_START_MEASUREMENT = '*OKOC'
    TAG_DC_CURRENT_OUT_OF_RANGE = '*OUT'
    TAG_DC_MEASUREMENT_OK = '*ADQ'

    REG_LENGTH = {
        CMD_AC_FHIGH_INT: 6,
        CMD_AC_FHIGH_DEC: 6,
        CMD_AC_FLOW_INT: 6,
        CMD_AC_FLOW_DEC: 6,
        CMD_AC_FPOINTS: 4,
        CMD_AC_AMP: 4,
        CMD_AC_SCAN_RATE: 1,
        CMD_AC_OCP_TYPE: 1,
        CMD_AC_START: 0,
        CMD_AC_STOP: 0,
        # COMANDOS DC
        CMD_DC_TYPE: 1,
        CMD_DC_ABS_LOWVOLT_POS: 4,
        CMD_DC_ABS_LOWVOLT_NEG: 4,
        CMD_DC_ABS_HIGHVOLT_POS: 4,
        CMD_DC_ABS_HIGHVOLT_NEG: 4,
        CMD_DC_REL_LOWVOLT_POS: 4,
        CMD_DC_REL_LOWVOLT_NEG: 4,
        CMD_DC_REL_HIGHVOLT_POS: 4,
        CMD_DC_REL_HIGHVOLT_NEG: 4,
        CMD_DC_SCANRATE: 6,
        CMD_DC_NPOINTS: 3,
        CMD_DC_START: 0,
        CDM_DC_STOP: 0
    }

    REG_MIN = {
        CMD_AC_FHIGH_INT: 0,
        CMD_AC_FHIGH_DEC: 0,
        CMD_AC_FLOW_INT: 0,
        CMD_AC_FLOW_DEC: 0,
        CMD_AC_FPOINTS: 1,
        CMD_AC_AMP: 10,
        CMD_AC_SCAN_RATE: 0,
        CMD_AC_OCP_TYPE: 0,
        # COMANDOS DC
        CMD_DC_TYPE: 0,
        CMD_DC_ABS_LOWVOLT_POS: 0,
        CMD_DC_ABS_LOWVOLT_NEG: 0,
        CMD_DC_ABS_HIGHVOLT_POS: 0,
        CMD_DC_ABS_HIGHVOLT_NEG: 0,
        CMD_DC_REL_LOWVOLT_POS: 0,
        CMD_DC_REL_LOWVOLT_NEG: 0,
        CMD_DC_REL_HIGHVOLT_POS: 0,
        CMD_DC_REL_HIGHVOLT_NEG: 0,
        CMD_DC_SCANRATE: 1,
        CMD_DC_NPOINTS: 1,
    }

    REG_MAX = {
        CMD_AC_FHIGH_INT: 100000,
        CMD_AC_FHIGH_DEC: 999,
        CMD_AC_FLOW_INT: 100000,
        CMD_AC_FLOW_DEC: 999,
        CMD_AC_FPOINTS: 1000,
        CMD_AC_AMP: 1000,
        CMD_AC_SCAN_RATE: 2,
        CMD_AC_OCP_TYPE: 1,
        # COMANDOS DC
        CMD_DC_TYPE: 3,
        CMD_DC_ABS_LOWVOLT_POS: 1999,
        CMD_DC_ABS_LOWVOLT_NEG: 1999,
        CMD_DC_ABS_HIGHVOLT_POS: 1999,
        CMD_DC_ABS_HIGHVOLT_NEG: 1999,
        CMD_DC_REL_LOWVOLT_POS: 1999,
        CMD_DC_REL_LOWVOLT_NEG: 1999,
        CMD_DC_REL_HIGHVOLT_POS: 1999,
        CMD_DC_REL_HIGHVOLT_NEG: 1999,
        CMD_DC_SCANRATE: 150000,
        CMD_DC_NPOINTS: 1000,
    }

    DC_TYPE_LINEAR = 'Lineal'
    DC_TYPE_CYCLIC = 'Cíclica'

    DC_TYPE_LINEAR_ASC = 'Lineal'
    DC_TYPE_LINEAR_DSC = 'Lineal Inversa'
    DC_TYPE_CYCLIC_ASC = 'Cíclica'
    DC_TYPE_CYCLIC_DSC = 'Cíclica Inversa'

    DC_ASC_DIRECTION = 1
    DC_DSC_DIRECTION = -1

    DC_REF_ABSOLUTE = 'Eref'
    DC_REF_RELATIVE = 'OCP'

    STR_DC_TYPE = {
        DC_TYPE_LINEAR_ASC: ARG_DC_LINEAR_POS,
        DC_TYPE_LINEAR_DSC: ARG_DC_LINEAR_NEG,
        DC_TYPE_CYCLIC_ASC: ARG_DC_CYCLIC_POS,
        DC_TYPE_CYCLIC_DSC: ARG_DC_CYCLIC_NEG
    }

    OCP_DYNAMIC = 'Dinámico'
    OCP_STATIC = 'Estático'

    STR_OCP = {
        OCP_DYNAMIC: ARG_AC_OCP_DYNAMIC,
        OCP_STATIC: ARG_AC_OCP_STATIC
    }

    SCANRATE_FAST = 'Rápido'
    SCANRATE_NORMAL = 'Normal'
    SCANRATE_SLOW = 'Lento'

    STR_AC_SCANRATE = {
        SCANRATE_FAST: ARG_AC_SCANRATE_FAST,
        SCANRATE_NORMAL: ARG_AC_SCANRATE_MID,
        SCANRATE_SLOW: ARG_AC_SCANRATE_SLOW
    }

    def __init__(self):
        # identification = CORRSTAT_NAME
        # Comandos de AC
        self._ac_freq_High_int = ''
        self._ac_freq_high_dec = ''
        self._ac_freq_Low_int = ''
        self._ac_freq_low_dec = ''
        self._ac_points = ''
        self._ac_amplitude = ''
        self._ac_scan_rate = ''
        self._ac_ocp = ''
        # Commandos de DC
        self._dc_type = ''
        self._dc_absolute_low_volt_pos = ''
        self._dc_absolute_low_volt_neg = ''
        self._dc_absolute_high_volt_pos = ''
        self._dc_absolute_high_volt_neg = ''
        self._dc_relative_low_volt_pos = ''
        self._dc_relative_low_volt_neg = ''
        self._dc_relative_high_volt_pos = ''
        self._dc_relative_high_volt_neg = ''
        self._dc_scan_rate = ''
        self._dc_points = ''

        # Registros high-level DC
        self.dc_high_volt_cmd = ''
        self.dc_low_volt_cmd = ''
        self.dc_seconds_per_point = 1

    def _validate_input(self, val, cmd):
        x = int(val)
        if x < self.REG_MIN[cmd] or x > self.REG_MAX[cmd]:
            raise ValueError('Valor "{}" del registro {} fuera de rango: [{},{}]'.format(
                x,
                cmd,
                self.REG_MIN[cmd],
                self.REG_MAX[cmd]
            ))
        else:
            xstr = str(x)
            if len(xstr) > self.REG_LENGTH[cmd]:
                raise ValueError('Valor "{}" del registro {} con mayor numero de caracteres: {}'.format(
                    x,
                    cmd,
                    cmd + '0'.zfill(self.REG_LENGTH[cmd]),
                ))
            else:
                return xstr.zfill(self.REG_LENGTH[cmd])

    # REGISTROS AC

    @property
    def rac_freq_High_int(self):
        return self._ac_freq_High_int

    @rac_freq_High_int.setter
    def rac_freq_High_int(self, val):
        self._ac_freq_High_int = self._validate_input(
            val, self.CMD_AC_FHIGH_INT)

    @property
    def rac_freq_high_dec(self):
        return self._ac_freq_high_dec

    @rac_freq_high_dec.setter
    def rac_freq_high_dec(self, val):
        self._ac_freq_high_dec = self._validate_input(
            val, self.CMD_AC_FHIGH_DEC)

    @property
    def rac_freq_Low_int(self):
        return self._ac_freq_Low_int

    @rac_freq_Low_int.setter
    def rac_freq_Low_int(self, val):
        self._ac_freq_Low_int = self._validate_input(val, self.CMD_AC_FLOW_INT)

    @property
    def rac_freq_low_dec(self):
        return self._ac_freq_low_dec

    @rac_freq_low_dec.setter
    def rac_freq_low_dec(self, val):
        self._ac_freq_low_dec = self._validate_input(val, self.CMD_AC_FLOW_DEC)

    @property
    def rac_points(self):
        return self._ac_points

    @rac_points.setter
    def rac_points(self, val):
        self._ac_points = self._validate_input(val, self.CMD_AC_FPOINTS)

    @property
    def ac_points(self):
        return int(self.rac_points)

    @ac_points.setter
    def ac_points(self, val):
        self.rac_points = val

    @property
    def rac_amplitude(self):
        return self._ac_amplitude

    @rac_amplitude.setter
    def rac_amplitude(self, val):
        self._ac_amplitude = self._validate_input(val, self.CMD_AC_AMP)

    @property
    def ac_amplitude(self):
        return float(self.rac_amplitude)

    @ac_amplitude.setter
    def ac_amplitude(self, val):
        self.rac_amplitude = val

    @property
    def rac_scan_rate(self):
        return self._ac_scan_rate

    @rac_scan_rate.setter
    def rac_scan_rate(self, val):
        self._ac_scan_rate = self._validate_input(val, self.CMD_AC_SCAN_RATE)

    @property
    def ac_scan_rate(self):
        sr = self.rac_scan_rate
        return list(self.STR_AC_SCANRATE.keys())[list(self.STR_AC_SCANRATE.values()).index(sr)]

    @ac_scan_rate.setter
    def ac_scan_rate(self, val):
        if not val:
            raise ValueError('No se reconoce la opción {}'.format(val))
        self.rac_scan_rate = self.STR_AC_SCANRATE[val]

    @property
    def rac_ocp(self):
        return self._ac_ocp

    @rac_ocp.setter
    def rac_ocp(self, val):
        self._ac_ocp = self._validate_input(val, self.CMD_AC_OCP_TYPE)

    @property
    def ac_ocp(self):
        sr = self.rac_ocp
        return list(self.STR_OCP.keys())[list(self.STR_OCP.values()).index(sr)]

    @ac_ocp.setter
    def ac_ocp(self, val):
        if not val:
            raise ValueError('No se reconoce la opción {}'.format(val))
        self.rac_ocp = self.STR_OCP[val]

    # REGISTROS DC

    @property
    def rdc_type(self):
        return self._dc_type

    @rdc_type.setter
    def rdc_type(self, val):
        self._dc_type = self._validate_input(val, self.CMD_DC_TYPE)

    @property
    def rdc_absolute_low_volt_pos(self):
        return self._dc_absolute_low_volt_pos

    @rdc_absolute_low_volt_pos.setter
    def rdc_absolute_low_volt_pos(self, val):
        self._dc_absolute_low_volt_pos = self._validate_input(
            val, self.CMD_DC_ABS_LOWVOLT_POS)

    @property
    def rdc_absolute_low_volt_neg(self):
        return self._dc_absolute_low_volt_neg

    @rdc_absolute_low_volt_neg.setter
    def rdc_absolute_low_volt_neg(self, val):
        self._dc_absolute_low_volt_neg = self._validate_input(
            val, self.CMD_DC_ABS_LOWVOLT_NEG)

    @property
    def rdc_absolute_high_volt_pos(self):
        return self._dc_absolute_high_volt_pos

    @rdc_absolute_high_volt_pos.setter
    def rdc_absolute_high_volt_pos(self, val):
        self._dc_absolute_high_volt_pos = self._validate_input(
            val, self.CMD_DC_ABS_HIGHVOLT_POS)

    @property
    def rdc_absolute_high_volt_neg(self):
        return self._dc_absolute_high_volt_neg

    @rdc_absolute_high_volt_neg.setter
    def rdc_absolute_high_volt_neg(self, val):
        self._dc_absolute_high_volt_neg = self._validate_input(
            val, self.CMD_DC_ABS_HIGHVOLT_NEG)

    @property
    def rdc_relative_low_volt_neg(self):
        return self._dc_relative_low_volt_neg

    @rdc_relative_low_volt_neg.setter
    def rdc_relative_low_volt_neg(self, val):
        self._dc_relative_low_volt_neg = self._validate_input(
            val, self.CMD_DC_REL_LOWVOLT_NEG)

    @property
    def rdc_relative_low_volt_pos(self):
        return self._dc_relative_low_volt_pos

    @rdc_relative_low_volt_pos.setter
    def rdc_relative_low_volt_pos(self, val):
        self._dc_relative_low_volt_pos = self._validate_input(
            val, self.CMD_DC_REL_LOWVOLT_POS)

    @property
    def rdc_relative_high_volt_neg(self):
        return self._dc_relative_high_volt_neg

    @rdc_relative_high_volt_neg.setter
    def rdc_relative_high_volt_neg(self, val):
        self._dc_relative_high_volt_neg = self._validate_input(
            val, self.CMD_DC_REL_HIGHVOLT_NEG)

    @property
    def rdc_relative_high_volt_pos(self):
        return self._dc_relative_high_volt_pos

    @rdc_relative_high_volt_pos.setter
    def rdc_relative_high_volt_pos(self, val):
        self._dc_relative_high_volt_pos = self._validate_input(
            val, self.CMD_DC_REL_HIGHVOLT_POS)

    @property
    def rdc_scan_rate(self):
        return self._dc_scan_rate

    @rdc_scan_rate.setter
    def rdc_scan_rate(self, val):
        self._dc_scan_rate = self._validate_input(val, self.CMD_DC_SCANRATE)

    @property
    def dc_points(self):
        return self._dc_points

    @dc_points.setter
    def dc_points(self, val):
        self._dc_points = self._validate_input(val, self.CMD_DC_NPOINTS)

    # INTERFACE PROPERTIES - HIGH LEVEL

    def _split_number(self, val, cmd):
        integer = int(val)
        decimal = int((val - integer) * 10 ** len(str(self.REG_MAX[cmd])))
        return integer, decimal

    @property
    def ac_max_freq(self):
        return float('{}.{}'.format(int(self.rac_freq_High_int), int(self.rac_freq_high_dec)))

    @ac_max_freq.setter
    def ac_max_freq(self, val):
        integer, decimal = self._split_number(val, self.CMD_AC_FHIGH_DEC)
        self.rac_freq_High_int = integer
        self.rac_freq_high_dec = decimal

    @property
    def ac_min_freq(self):
        return float('{}.{}'.format(int(self.rac_freq_Low_int), int(self.rac_freq_low_dec)))

    @ac_min_freq.setter
    def ac_min_freq(self, val):
        integer, decimal = self._split_number(val, self.CMD_AC_FLOW_DEC)
        self.rac_freq_Low_int = integer
        self.rac_freq_low_dec = decimal

    @property
    def gen_ac_config(self):
        config = [self.CMD_AC_FHIGH_INT + self.rac_freq_High_int + self.TAG_EOL,  # Frecuencia Máxima parte entera
                  self.CMD_AC_FHIGH_DEC + self.rac_freq_high_dec + \
                  self.TAG_EOL,  # Frecuencia Máxima parte decimal
                  self.CMD_AC_FLOW_INT + self.rac_freq_Low_int + \
                  self.TAG_EOL,    # Frecuencia Mínima parte entera
                  self.CMD_AC_FLOW_DEC + self.rac_freq_low_dec + \
                  self.TAG_EOL,    # Frecuencia Mínima parte decimal
                  self.CMD_AC_AMP + self.rac_amplitude + self.TAG_EOL,            # Amplitud
                  self.CMD_AC_FPOINTS + self.rac_points + \
                  self.TAG_EOL,           # Número de puntos
                  self.CMD_AC_OCP_TYPE + self.rac_ocp + self.TAG_EOL,             # Tipo de OCP
                  self.CMD_AC_SCAN_RATE + self.rac_scan_rate + self.TAG_EOL]      # Velocidad de barrido
        return config

    def dc_set_registers(self, scan_type, initial_potential_mv, final_potential_mv, volt_ref, points, scanrate_uvps):

        self.dc_points = points
        self.dc_seconds_per_point = (abs(final_potential_mv - initial_potential_mv)/(scanrate_uvps/1000))/points

        self.rdc_scan_rate = scanrate_uvps

        sweep_direction = self.DC_DSC_DIRECTION if initial_potential_mv > final_potential_mv else self.DC_ASC_DIRECTION
        if scan_type == self.DC_TYPE_LINEAR and sweep_direction == self.DC_ASC_DIRECTION:
            self.rdc_type = self.ARG_DC_LINEAR_POS
        elif scan_type == self.DC_TYPE_LINEAR and sweep_direction == self.DC_DSC_DIRECTION:
            self.rdc_type = self.ARG_DC_LINEAR_NEG
        elif scan_type == self.DC_TYPE_CYCLIC and sweep_direction == self.DC_ASC_DIRECTION:
            self.rdc_type = self.ARG_DC_CYCLIC_POS
        elif scan_type == self.DC_TYPE_CYCLIC and sweep_direction == self.DC_DSC_DIRECTION:
            self.rdc_type = self.ARG_DC_CYCLIC_NEG

        vmin = min([initial_potential_mv, final_potential_mv])
        vmax = max([initial_potential_mv, final_potential_mv])


        if volt_ref == self.DC_REF_ABSOLUTE:
            if vmin < 0:
                self.rdc_absolute_low_volt_neg = abs(vmin)
                self.dc_low_volt_cmd = self.CMD_DC_ABS_LOWVOLT_NEG + \
                    self.rdc_absolute_low_volt_neg + self.TAG_EOL
            else:
                self.rdc_absolute_low_volt_pos = vmin
                self.dc_low_volt_cmd = self.CMD_DC_ABS_LOWVOLT_POS + \
                    self.rdc_absolute_low_volt_pos + self.TAG_EOL

            if vmax < 0:
                self.rdc_absolute_high_volt_neg = abs(vmax)
                self.dc_high_volt_cmd = self.CMD_DC_ABS_HIGHVOLT_NEG + \
                    self.rdc_absolute_high_volt_neg + self.TAG_EOL
            else:
                self.rdc_absolute_high_volt_pos = abs(vmax)
                self.dc_high_volt_cmd = self.CMD_DC_ABS_HIGHVOLT_POS + \
                    self.rdc_absolute_high_volt_pos + self.TAG_EOL
        else:
            if vmin < 0:
                self.rdc_relative_low_volt_neg = abs(vmin)
                self.dc_low_volt_cmd = self.CMD_DC_REL_LOWVOLT_NEG + \
                    self.rdc_relative_low_volt_neg + self.TAG_EOL
            else:
                self.rdc_relative_low_volt_pos = abs(vmin)
                self.dc_low_volt_cmd = self.CMD_DC_REL_LOWVOLT_POS + \
                    self.rdc_relative_low_volt_pos + self.TAG_EOL

            if vmax < 0:
                self.rdc_relative_high_volt_neg = abs(vmax)
                self.dc_high_volt_cmd = self.CMD_DC_REL_HIGHVOLT_NEG + \
                    self.rdc_relative_high_volt_neg + self.TAG_EOL
            else:
                self.rdc_relative_high_volt_pos = abs(vmax)
                self.dc_high_volt_cmd = self.CMD_DC_REL_HIGHVOLT_POS + \
                    self.rdc_relative_high_volt_pos + self.TAG_EOL

    @property
    def gen_dc_config(self):
        config = [
            self.CMD_DC_TYPE + self.rdc_type + self.TAG_EOL,
            self.dc_high_volt_cmd,
            self.dc_low_volt_cmd,
            self.CMD_DC_SCANRATE + self.rdc_scan_rate + self.TAG_EOL,
            self.CMD_DC_NPOINTS + self.dc_points + self.TAG_EOL,
        ]
        return config


class PortClosedDuringCmdExecution(Exception):
    pass


class CorrStat(CorrStatRegs):
    """
    CorrStat:
    Objeto que representa y maneja un potenciostato CorrStat
    """
    PARAM_AC_FREQ_MAX = 'freq_max_hz'
    PARAM_AC_FREQ_MIN = 'freq_min_hz'
    PARAM_AC_POINTS = 'points'
    PARAM_AC_AMPLITUDE = 'amplitude_mv'
    PARAM_AC_OCP_TYPE = 'ocp'
    PARAM_AC_SENS = 'sens'

    # DC Parameters
    PARAM_DC_INITIAL_POTENTIAL = 'initial_potential_mv'
    PARAM_DC_FINAL_POTENTIAL = 'final_potential_mv'
    PARAM_DC_POTENTIAL_REFERENCE = 'volt_ref'  # Eref: absolute, Eoc: Relative
    PARAM_DC_POINTS = 'points'
    PARAM_DC_TYPE = 'scan_type'  # linear, cyclic
    PARAM_DC_SCANRATE = 'scanrate_uvps'

    # TODO definir parámetros de las técnicas DC

    NEXT = '>'.encode()
    REPEAT = '<'.encode()

    AC_DATA_FIELDS = collections.OrderedDict({
        'ocp_flag': '>4s',
        'dc_current_mA': '>f',
        'dc_current_flag': '>4s',
        'ocp_potential_mV': '>f',
        'range_flag': '>4s',
        'frequency': '>f',
        'magnitude': '>f',
        'phase': '>f',
        #        'real',
        #        'imaginary',
        'voltage_signal': '>2048s',
        'current_signal': '>2048s',
        'period': '>f',
        'gain1': '>2c',
        'gain2': '>2c',
        'gain3': '>2c'

    })

    DC_DATA_FIELDS = collections.OrderedDict({
        'adq_flag': '>5s',
        'dc_potential_mV': '>f',
        'dc_current_mA': '>f',
        'gain_flag': '>2s',
        'potential_gain': '>b',
        'current_gain': '>b',
    })

    DC_OKOC_DATA_FIELDS = collections.OrderedDict({
        'okoc_flag': '>4s',
        'ocp_potential_mV': '>f',
    })

    def __init__(self, port=None, name=CORRSTAT_NAME):
        super().__init__()
        self.name = name
        self.port = ''
        self.comm = None

        self.connect(port)

    def set_ac_config(self, params):
        """
        :param params:  diccionario con los parámetros de ac
        :return: None
        """
        self.ac_scan_rate = params[self.PARAM_AC_SENS]
        self.ac_ocp = params[self.PARAM_AC_OCP_TYPE]
        self.ac_points = params[self.PARAM_AC_POINTS]
        self.ac_amplitude = params[self.PARAM_AC_AMPLITUDE]
        self.ac_max_freq = params[self.PARAM_AC_FREQ_MAX]
        self.ac_min_freq = params[self.PARAM_AC_FREQ_MIN]

    def set_dc_config(self, params):
        """
        :param params:  diccionario con los parámetros de ac
        :return: None
        """
        self.dc_set_registers(
            params[self.PARAM_DC_TYPE],                 # scan_type
            params[self.PARAM_DC_INITIAL_POTENTIAL],    # initial_potential 
            params[self.PARAM_DC_FINAL_POTENTIAL],      # final_potential, 
            params[self.PARAM_DC_POTENTIAL_REFERENCE],  # volt_ref, 
            params[self.PARAM_DC_POINTS],               # points, 
            params[self.PARAM_DC_SCANRATE]              # scan_rate
        )

    def send_config(self, config_commands: list = None):
        if config_commands:
            for cmd in config_commands:
                self.send_cmd(cmd, cmd, retry=5, eol=self.TAG_EOL)

    def send_ac_config(self):
        self.send_config(self.gen_ac_config)

    def check_comm(self, com_ser):
        if com_ser is not None:
            self.comm = com_ser
            if self.is_connected:
                return True
            else:
                self.comm = None
                return False

    def connect(self, port=None):
        if port is not None:
            comm = open_serial_port(port)
            return self.check_comm(comm)
        else:
            ports = serial_ports()
            for p in ports:
                comm = open_serial_port(p)
                if self.check_comm(comm):
                    return True
        return False

    def close_port(self):
        self.comm.close()

    @property
    def is_connected(self):
        """
        Verifica si el sistema está conectado y responde al comando de identificación
        :return: Retorna True si el sistema está conectado y responde al comando de identificación
        de lo contrario retorna False
        """
        if self.comm is None:
            return False
        # return self.send_cmd(self.CMD_IDENTIFY, self.TAG_ID)
        return self.comm.is_open

    @property
    # TODO: Confirmar si ambas (AC y DC) pruebas se detienen con el mismo comando.
    def is_ready(self):
        """
        Verifica que el sistema esté listo para recibir comandos. Si no está listo, intenta llevarlo a un estado
        conocido :return: Retorna True si el sistema está conectado y responde al comando de identificación. Si falla
        al identificarse, inicia el proceso para detener las pruebas y asegura que el sistema vuelva a un estado
        IDLE. Retorna False si el sistema no responde adecuadamente luego de 120 intentos.
        """

        if self.is_connected:
            cmd_ok = self.send_cmd(
                self.CMD_IDENTIFY + self.TAG_EOL, self.TAG_ID + self.TAG_EOL, eol=self.TAG_EOL)
            # print(f"inside is ready: {cmd_ok}")
            if cmd_ok:
                return True
        return False

    @property
    def is_busy(self):
        return not self.is_ready

    def send_cmd(self, cmd, response='', **kwargs):
        retry = 1
        eol = ''

        for key, value in kwargs.items():
            if key == 'retry':
                retry = value
            if key == 'eol':
                eol = value

        if not self.comm.is_open:
            raise PortClosedDuringCmdExecution

        # Clean buffer
        self.comm.reset_input_buffer()
        self.comm.reset_output_buffer()
        self.comm.flush()
        # Send Command
        for r in range(0, retry):
            self.comm.write(cmd.encode())
            self.comm.flush()
            # Receive response
            if eol:
                res = bytearray(self.comm.read_until(
                    eol.encode(), len(response)))
            else:
                res = bytearray(self.comm.read(len(response)))
            # print('command sent')
            if response:
                while res:
                    if res == response.encode():
                        print(
                            'cmd: {} -> res: {} = {}'.format(cmd, res, response))
                        while res:
                            res = self.comm.read()
                        return True
                    else:
                        if eol:
                            res = self.comm.read_until(
                                eol.encode(), len(response))
                        else:
                            res = self.comm.read(len(response))
                            print(f"res: {res} ? {response}")
            else:
                return False
        return False

    def estimate_next_freq(self, point: int) -> tuple:
        """
        Calcula la frecuencia del siguiente dato a leer
        :param point: El número de punto actual
        :return: Frecuencia del siguiente punto
        """
        delta = (math.log10(self.ac_max_freq) -
                 math.log10(self.ac_min_freq)) / (self.ac_points - 1)
        freq = 10 ** (math.log10(self.ac_max_freq) - delta * point)
        fs = freq / 100
        return freq, fs

    def estimate_timeleft(self, point):
        total_points = self.ac_points
        duration = 0
        for p in range(point+1, total_points):
            freq, _ = self.estimate_next_freq(p)
            duration += estimate_point_duration(freq)
        return duration

    def read_ac_data_block(self, retries=10, timeout=1) -> bytearray:
        byte_res = bytearray()
        size = self._calc_ac_field_size()

        start_trx = False
        for retry in range(0, retries):
            self.comm.timeout = timeout
            self.comm.write(self.NEXT)
            res = self.comm.read(size)
            if b'OKOC' in res:
                start_trx = True
            if len(res):
                if start_trx:
                    byte_res.extend(bytearray(res))
            else:
                print('Intento ', retry, byte_res)

            if len(byte_res) >= size:
                idx = byte_res.find(b'OKOC')
                return byte_res[idx:]

        if len(byte_res) < size:
            raise ConnectionError('No se obtuvo respuesta')

    def _calc_ac_field_size(self):
        data_fmt = '>' + \
            (''.join(self.AC_DATA_FIELDS.values())).replace('>', '')
        return struct.calcsize(data_fmt)

    def read_dc_data_block(self, isOKOC=False, retries=20, timeout=1) -> bytearray:
        byte_res = bytearray()
        size = self._calc_dc_field_size(isOKOC=isOKOC)
        if not isOKOC:
            flag = b"*ADQ;"
        else:
            flag = b"OKOC"
        error_flag = b"*OUT;"
        idx = 0
        received_bytes = 0

        start_trx = False
        for retry in range(0, retries):
            self.comm.timeout = timeout
            self.comm.write(self.NEXT)
            res = self.comm.read(size-received_bytes)

            if error_flag in res:
                return error_flag

            if flag in res:
                start_trx = True

            print("res: ", res)

            if len(res):
                if start_trx:
                    byte_res.extend(bytearray(res))
                    idx = byte_res.find(flag)
            else:
                print('Intento ', retry, byte_res.hex())
            received_bytes = len(byte_res)-idx
            if received_bytes == size:
                return byte_res[idx:]

        if len(byte_res) < size:
            raise ConnectionError('No se obtuvo respuesta')

    def _calc_dc_field_size(self, isOKOC=False):
        if not isOKOC:
            data_fmt = '>' + \
                (''.join(self.DC_DATA_FIELDS.values())).replace('>', '')
            return struct.calcsize(data_fmt)
        else:
            data_fmt = '>' + \
                (''.join(self.DC_OKOC_DATA_FIELDS.values())).replace('>', '')
            return struct.calcsize(data_fmt)


    def start(self):
        time.sleep(1)
        return self.send_cmd(self.CMD_AC_START + self.TAG_EOL, self.TAG_AC_STARTED + self.TAG_EOL)

    def get_dc_timeleft(self, current_point):
        return self.dc_seconds_per_point*int(self.dc_points) - self.dc_seconds_per_point*current_point

    @property
    def dc_retries(self):
        if self.dc_seconds_per_point < 10:
            return 20
        else:
            return int(self.dc_seconds_per_point*10)

    def read_dc_data(self, fmt_fcn=None, *fmt_fcn_args, **fmt_fcn_kwargs):
        has_started = self.send_cmd(self.CMD_DC_START + self.TAG_EOL,
                                    self.TAG_DC_START_SCAN, eol=self.TAG_EOL,
                                    retry=3)

        okoc_data_block = self.read_dc_data_block(isOKOC=True)
        print("OKOC data block: ", okoc_data_block.hex())
        data = self.decode_dc_data_block(okoc_data_block, isOKOC=True)
        yield data
        for point in range(0, int(self.dc_points)):
            data_block = self.read_dc_data_block(retries=self.dc_retries)
            data = self.decode_dc_data_block(data_block)
            data['progress'] = round(((point + 1) / int(self.dc_points)) * 100)
            data['timeleft'] = self.get_dc_timeleft(point)
            yield data

    def decode_dc_data_block(self, data_block, isOKOC=False):
        if data_block == "*OUT;":
            return {'error': "Se presentó aumento de corriente que excede la capacidad. Se detiene el proceso de medición"}

        if not isOKOC:
            format = self.DC_DATA_FIELDS
        else:
            format = self.DC_OKOC_DATA_FIELDS

        data = {}
        for field, fmt in format.items():
            size = struct.calcsize(fmt)
            block = data_block[0:size]
            value = struct.unpack(fmt, block)
            data[field] = format_dc_data(value, field, isOKOC=isOKOC)
            del data_block[0:size]
        return data

    def read_ac_data(self, fmt_fcn=None, *fmt_fcn_args, **fmt_fcn_kwargs):
        """
        Lee los datos del potenciostato y los devuelve uno a uno (yield) con un formato definido.
        :param fmt_fcn: Función que aplica el formato a cada campo de datos de la estructura.
         debe tener la siguiente firma: fmt_fcn(value, field, *fmt_fcn_args, **fmt_fcn_kwargs)
         Si no se define un formato, se retorna con el formato por defecto de la función struct.unpack()
         Por defecto es fmt_fcn=None.
        :param fmt_fcn_args: argumentos a pasar a la función de formato de datos
        :param fmt_fcn_kwargs: argumentos a pasar a la función de formato de datos
        :return data: Un diccionario con la estructura definida
        """
        # Start the measurement routine
        has_started = self.send_cmd(self.CMD_AC_START + self.TAG_EOL,
                                    self.TAG_AC_STARTED + self.TAG_EOL, eol=self.TAG_EOL,
                                    retry=3)
        print('Start the measurement routine')
        for point in range(0, self.ac_points):
            if not has_started:
                print('*** No inició la medición ***')
                return False
            freq, _ = self.estimate_next_freq(point)
            retries = estimate_retries(estimate_point_duration(freq))
            data = {}
            data_block = self.read_ac_data_block(retries=retries, timeout=5)
            if data_block is not None:
                for field, fmt in self.AC_DATA_FIELDS.items():
                    size = struct.calcsize(fmt)
                    block = data_block[0:size]
                    value = struct.unpack(fmt, block)
                    del data_block[0:size]
                    if fmt_fcn:
                        try:
                            data[field] = fmt_fcn(
                                value, field, *fmt_fcn_args, **fmt_fcn_kwargs)
                        except:
                            print(value)
                    else:
                        data[field] = value
                data['progress'] = round(((point + 1) / self.ac_points) * 100)
                data['timeleft'] = self.estimate_timeleft(point)
            yield data

    def stop(self, stop_cmd='R', retries=120, sleep=1, timeout=1):
        byte_res = bytearray()
        size = self._calc_ac_field_size()

        self.comm.timeout = timeout
        for retry in range(0, retries):
            self.comm.reset_input_buffer()
            self.comm.reset_output_buffer()
            self.comm.flush()
            self.comm.write(stop_cmd.encode())
            self.comm.flush()
            if self.comm.in_waiting > 0:
                self.comm.read()
            print(f"retry stop>>> {retry}")
            time.sleep(2)
            if self.is_ready:
                return True
        return False

    def get_uid(self):
        if self.is_ready:
            cmd = self.CMD_UID + self.TAG_EOL
            self.comm.write(cmd.encode())
            self.comm.flush()
            res = self.comm.read_until(
                self.TAG_EOL.encode(), len("MC0001uid;"))
            return res.decode().replace(cmd, '')


# TODO: agregar '*args, **kwargs' para editar las funciones y formatos
def format_ac_data(value, field):
    def to_string(x):
        return x[0].decode()

    def to_hex(x):
        return bytes(x[0]).hex()

    def to_number(x):
        return float(x[0])

    def to_list(x):
        block_bytes, = x
        block_bytes = bytearray(block_bytes)
        lsb = block_bytes[0:1024]
        msb = block_bytes[1024:2048]

        y = []
        for k in range(0, 1024):
            b = msb[k].to_bytes(1, byteorder='big') + \
                lsb[k].to_bytes(1, byteorder='big')
            y.append(int.from_bytes(b, byteorder='big'))
        return y

    fmt_data = {
        'dc_current_flag': to_string,
        'dc_current_mA': to_number,
        'ocp_flag': to_string,
        'ocp_potential_mV': to_number,
        'range_flag': to_string,
        'frequency': to_number,
        'magnitude': to_number,
        'phase': to_number,
        #        'real',
        #        'imaginary',
        'voltage_signal': to_list,
        'current_signal': to_list,
        'period': to_number,
        'gain1': to_hex,
        'gain2': to_hex,
        'gain3': to_hex,
    }
    return fmt_data[field](value)

def format_dc_data(value, field, isOKOC):
    def to_string(x):
        return x[0].decode()

    def to_float(x):
        return float(x[0])

    def to_integer(x):
        return int(x[0])

    if not isOKOC:
        fmt_data = {
            'adq_flag': to_string,
            'dc_potential_mV': to_float,
            'dc_current_mA': to_float,
            'gain_flag': to_string,
            'potential_gain': to_integer,
            'current_gain': to_integer,
        }
    else:
        fmt_data = {
            'okoc_flag': to_string,
            'ocp_potential_mV': to_float,
        }
    return fmt_data[field](value)


def write_progress_log(progress, **fcn_kws):
    filename = 'CorrStat_log.txt'
    for key, value in fcn_kws.items():
        if key == 'filename':
            filename = value

    with open(filename, 'a') as file:
        file.write(json.dumps(progress, ident=4))


def estimate_point_duration(frequency):
    secs = 10
    # Espera Inicial
    if frequency > 1000:
        secs += 20
    elif frequency > 1:
        secs += (10.0/frequency)
    elif frequency > 0.01:
        secs += (0.5/frequency)
    else:
        secs += 65

    # Espera medición
    if frequency >= 1100:
        secs += (204.8/frequency)
    else:
        secs += 20 + (256*(1/frequency)/20)*100

    # Espera cálculos
    secs += 2
    return secs


def estimate_retries(duration_secs: float, base_time: float = 1.0) -> int:
    return round(duration_secs / base_time) * 3


if __name__ == '__main__':
    # tty_port = '/dev/ttyUSB0'
    # crea un objeto tipo CorrStat(), que representa al hardware conectado
    # al puerto indicado e intenta una conexión.
    # try:
    #     cstat = CorrStat(port=tty_port)
    # except:
    #     print('No se pudo conectar al puerto {}'.format(tty_port))

    # cstat = CorrStat('/dev/ttyUSB0')
    cstat = CorrStat()
    print('Inicializado')

    # Configuración de prueba AC
    # Las opciones de los parámetros ocp y sens, se encuentran en los diccionarios
    # CorrStat.STR_OCP y CorrStat.STR_AC_SCANRATE respectivamente
    eis_params = {
        'freq_max_hz': 1,
        'freq_min_hz': 0.05,
        'points': 3,
        'amplitude_mv': 20,
        'ocp': CorrStat.OCP_STATIC,
        'sens': CorrStat.SCANRATE_NORMAL
    }

    # if cstat.is_ready:
    #     cstat.set_ac_config(eis_params)
    #     cstat.send_ac_config()
    #     g = cstat.read_ac_data(format_data)
    #     print(next(g))
    #     cstat.stop()
    #     if cstat.is_ready:
    #         print("I Won! Go drink a beer...")


# ---------------------------------------------------------------------------------------------------

    # Verifica que el potenciostato esté listo para recibir comandos
    if cstat.is_ready:
        try:
            # Valida los parámetros proporcionados, y actualiza los registros del objeto CorrStatRegs
            cstat.set_ac_config(eis_params)

            # Envía los comandos de configuración al potenciostatos validando las respuestas
            cstat.send_ac_config()
        except Exception as e:
            print(e)
    else:
        print('Potenciostato no responde')

    # En cada iteración espera y recibe un punto medido y analizado por la máquina.
    # El loop se ejecuta el numero de veces configurado en el parámetro points
    start_time = time.time()
    start_point_time = time.time()

    for current_point, data_ in enumerate(cstat.read_ac_data(format_ac_data)):
        point_time = time.time() - start_point_time
        start_point_time = time.time()
        total_time = time.time() - start_time
        try:
            msg = '{0}\t{1:10.5f}\t{2:3d}%\tEstimado:{3}\tPunto: {4}\tTotal: {5}'.format(
                current_point,
                data_['frequency'],
                data_['progress'],
                time.strftime('%H:%M:%S', time.gmtime(data_['timeleft'])),
                time.strftime('%H:%M:%S', time.gmtime(point_time)),
                time.strftime('%H:%M:%S', time.gmtime(total_time)),
            )
            print(msg)
        except:
            print(data_)

# ---------------------------------------------------------------------------------------------------

    # cstat = CorrStat()
    #
    # eis_params = {
    #     'freq_max_Hz': 100000,
    #     'freq_min_Hz': 10000,
    #     'points': 50,
    #     'amplitude_mV': 20,
    #     'ocp': 'Dinámico',
    #     'sens': 'Normal'
    # }
    #
    # cstat.connect('/dev/ttyUSB0')
    #
    # if cstat.is_connected:
    #     print('Conectado')
    # else:
    #     if cstat.stop():
    #         print('Conectado')
    #     else:
    #         raise NameError('No hay comunicación')

    # print('*** Prueba  actualizar frecuencia máxima ***')
    # corrstat.regs.rac_freq_High_int = '10000'
    # corrstat.regs.rac_freq_high_dec = '333'
    # print('ac_max_freq: ', corrstat.regs.ac_max_freq)
    #
    # corrstat.regs.ac_max_freq = 003390.4444444444444
    # print('ac_max_freq: ', corrstat.regs.ac_max_freq)
    #
    # print('*** Prueba  actualizar frecuencia mínima ***')
    # corrstat.regs.ac_min_freq = 0.33333
    # print('ac_min_freq: ', corrstat.regs.ac_min_freq)
    #
    # print('*** Prueba  actualizar scan rate ***')
    # corrstat.regs.ac_scan_rate = CorrStat.SCANRATE_FAST
    # print('Scan Rate: ', corrstat.regs.ac_scan_rate)
    # corrstat.regs.ac_scan_rate = CorrStat.SCANRATE_NORMAL
    # print('Scan Rate: ', corrstat.regs.ac_scan_rate)
    # corrstat.regs.ac_scan_rate = CorrStat.SCANRATE_SLOW
    # print('Scan Rate: ', corrstat.regs.ac_scan_rate)
    #
    # print('*** Prueba  actualizar tipo de OCP ***')
    # corrstat.regs.ac_ocp = CorrStat.OCP_DYNAMIC
    # print('Tipo de OCP: ', corrstat.regs.ac_ocp)
    # corrstat.regs.ac_ocp = CorrStat.OCP_STATIC
    # print('Tipo de OCP: ', corrstat.regs.ac_ocp)
    #
    # print('*** Prueba  actualizar generar rutina AC ***')
    # corrstat.regs.ac_max_freq = 100e3
    # corrstat.regs.ac_min_freq = 1e-2
    # corrstat.regs.ac_amplitude = 10
    # corrstat.regs.ac_points = 20
    # corrstat.regs.ac_ocp = CorrStat.OCP_STATIC
    # corrstat.regs.ac_scan_rate = CorrStat.SCANRATE_NORMAL
    # config = corrstat.regs.gen_ac_config()
    # for c in config:
    #     print(c)

    # print('*** Prueba  generar rutina AC ***')
    # corrstat.ac_max_freq = 100e3
    # corrstat.ac_min_freq = 1e-2
    # corrstat.ac_amplitude = 10
    # corrstat.ac_points = 20
    # corrstat.ac_ocp = CorrStat.OCP_STATIC
    # corrstat.ac_scan_rate = CorrStat.SCANRATE_NORMAL
    # config = corrstat.gen_ac_config()
    # for c in config:
    #     print(c)

    # print('*** Prueba  configurar rutina AC desde dict ***')
    #
    # cstat.set_ac_config(eis_params)

    # config = corrstat.gen_ac_config()
    # for c in config:
    #     print(c)

    # cstat.send_ac_config()
    # for data in cstat.read_ac_data(format_data):
    #     print(data)
