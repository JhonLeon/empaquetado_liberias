from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.http import FileResponse
import threading
import serial
from pathlib import Path

# Models
from .models import Device
from .models import ExperimentStatus
from .models import Technique
from .models import Experiment
from .models import ExperimentConfig
from .models import ExperimentConfigStorage
from .models import Data
from .models import ExperimentEqua
from .models import Material
# Serilizers
from .serializers import DeviceSerializer
from .serializers import ExperimentStatusSerializer
from .serializers import TechniqueSerializer
from .serializers import ExperimentSerializer
from .serializers import ExperimentConfigSerializer
from .serializers import ExperimentConfigStorageSerializer
from .serializers import DataSerializer
from .serializers import ExperimentEquaSerializer
from .serializers import MaterialExportSerializer
# Export Serializer
from .serializers import ExperimentExportSerializer
from .serializers import ExperimentConfigExportSerializer
from .serializers import ExperimentStatusExportSerializer
from .serializers import DataExportSerializer
# Scripts
from .scripts import start_corrstat_experiment
from .scripts import detect_corrstat_device
from .scripts import delete_unconnected_corrstat_devices
from .scripts import hard_stop_all_devices
from .scripts import list2chartjs
from .scripts import get_experiment_info
from .scripts import insert_imported_data_to_db
from .scripts import generate_experiment_export_file
from .scripts import generate_experiments_zip_file
# Parsers
from .parsers import ExperimentParser
# CIC libraries
from corrstatlink import CorrStat
import impexp


class DeviceList(generics.ListAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer


class DeviceDetail(generics.RetrieveAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

class ListMaterial(generics.ListAPIView):
    queryset = Material.objects.all()
    serializer_class = MaterialExportSerializer


class ExperimentStatusListView(generics.ListAPIView):
    queryset = ExperimentStatus.objects.all()
    serializer_class = ExperimentStatusSerializer


class ExperimentStatusDetailView(generics.RetrieveAPIView):
    queryset = ExperimentStatus.objects.all()
    serializer_class = ExperimentStatusSerializer


class TechniqueListView(generics.ListAPIView):
    queryset = Technique.objects.all()
    serializer_class = TechniqueSerializer


class ExperimentListView(generics.ListAPIView):
    # queryset = Experiment.objects.all()
    serializer_class = ExperimentSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Experiment.objects.all()
        return Experiment.objects.filter(user_id=user)


class ExperimentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Experiment.objects.all().order_by()
    serializer_class = ExperimentSerializer


class ExperimentConfigListView(generics.ListAPIView):
    queryset = ExperimentConfig.objects.all()
    serializer_class = ExperimentConfigSerializer

class ExperimentConfigEqua(generics.ListAPIView):
    queryset = ExperimentEqua.objects.all()
    serializer_class = ExperimentEquaSerializer

class ExperimentConfigStorageListView(generics.ListAPIView):
    queryset = ExperimentConfigStorage.objects.all()
    serializer_class = ExperimentConfigStorageSerializer


class ExperimentConfigDetailView(generics.RetrieveAPIView):
    queryset = ExperimentConfig.objects.all()
    serializer_class = ExperimentConfigSerializer


class ExperimentConfigStorageDetailView(generics.RetrieveDestroyAPIView):
    queryset = ExperimentConfigStorage.objects.all()
    serializer_class = ExperimentConfigStorageSerializer


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def add_experiment_config_to_storage(request, experiment_id):
    experiment = get_object_or_404(
        Experiment, pk=experiment_id, user_id=request.user)
    experiment_config = get_object_or_404(
        ExperimentConfig, experiment_id=experiment.id)
    

    if experiment.technique_tag.technique == "EIS":
        tag = request.data['tag']

        ExperimentConfigStorage.objects.create(
            freq_max_hz=experiment_config.freq_max_hz,
            freq_min_hz=experiment_config.freq_min_hz,
            amplitude_mv=experiment_config.amplitude_mv,
            ocp=experiment_config.ocp,
            sens=experiment_config.sens,
            dc_type=experiment_config.dc_type,
            initial_potential_mv=experiment_config.initial_potential_mv,
            final_potential_mv=experiment_config.final_potential_mv,
            scan_rate=experiment_config.scan_rate,
            points=experiment_config.points,
            tag=tag,
            etiqueta = experiment.technique_tag.technique
            )

        save_config = ExperimentConfigStorage.objects.last()
        save_config_ser = ExperimentConfigStorageSerializer(save_config)

        return Response(save_config_ser.data)
    
    elif experiment.technique_tag.technique != "EIS":
        
        experiment_equa = get_object_or_404(
        ExperimentEqua, experiment_id=experiment.id)

        tag = request.data['tag']

        ExperimentConfigStorage.objects.create(
            freq_max_hz=experiment_config.freq_max_hz,
            freq_min_hz=experiment_config.freq_min_hz,
            amplitude_mv=experiment_config.amplitude_mv,
            ocp=experiment_config.ocp,
            sens=experiment_config.sens,
            dc_type=experiment_config.dc_type,
            initial_potential_mv=experiment_config.initial_potential_mv,
            final_potential_mv=experiment_config.final_potential_mv,
            scan_rate=experiment_config.scan_rate,
            points=experiment_config.points,
            tag=tag,
            area=experiment_equa.area,
            beta=experiment_equa.beta,
            material=experiment_equa.material,
            etiqueta = experiment.technique_tag.technique
            )

        save_config = ExperimentConfigStorage.objects.last()
        save_config_ser = ExperimentConfigStorageSerializer(save_config)

        return Response(save_config_ser.data)


@api_view(http_method_names=['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def experiment_info_view(request, pk):
    if request.user.is_staff:
        experiment = get_object_or_404(Experiment, pk=pk)
    else:
        experiment = get_object_or_404(Experiment, pk=pk, user_id=request.user)
    return Response(get_experiment_info(experiment))


@api_view(http_method_names=['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def retrive_experiment_device(request, pk):
    device = get_object_or_404(Device, experiment_id=pk)
    device_ser = DeviceSerializer(device)
    return Response(device_ser.data)


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_start(request, pk):
    # experiment_config = JSONParser().parse(request)
    config = request.data
    device = get_object_or_404(Device, pk=pk)

    alive_threads = threading.enumerate()
    for t in alive_threads:
        if t.name == device.uid:
            print(f'>>>>> Thread {t.name} is still running')
            return Response({'message': f'Experiment routine is running on device {device.uid}'})

    if device.status == 'ready':
        # Create Experiment
        experiment = Experiment.objects.create(
            name=config['name'],
            comment=config['comment'],
            user_id=User.objects.get(pk=config['user_id']),
            technique_tag=Technique.objects.get(
                technique=config['technique_tag'])
        )
        device.status = 'busy'
        device.experiment_id = experiment.pk
        device.save()
        experiment_thread = threading.Thread(
            target=start_corrstat_experiment,
            args=(device.uid, config, experiment),
            daemon=False,
            name=device.uid
        )
        print('~~~~~~~ Start Experiment Thread ~~~~~~~')
        experiment_thread.start()
        return Response({
            "message": f"Experiment Running",
            "experiment_id": experiment.pk
        })

    return Response({"message": f"Device not Ready"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(http_method_names=['PUT'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_pause(request, pk):
    device = get_object_or_404(Device, pk=pk)
    if device.status == 'busy':
        device.action = 'pause'
        device.save()
        return Response({"message": "Pause action received"})
    return Response({"message": "Device not in busy state"}, status=status.HTTP_409_CONFLICT)


@api_view(http_method_names=['PUT'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_resume(request, pk):
    device = get_object_or_404(Device, pk=pk)
    if device.status == 'paused':
        device.action = 'resume'
        device.save()
        return Response({"message": "Resume action received"})
    return Response({"message": "Device not in paused state"}, status=status.HTTP_409_CONFLICT)


@api_view(http_method_names=['PUT'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_stop(request, pk):
    device = get_object_or_404(Device, pk=pk)
    device.action = 'stop'
    device.save()

    if device.experiment_id > 0:
        experiment_status = get_object_or_404(
            ExperimentStatus, experiment_id=device.experiment_id)
    else:
        experiment_status = None

    # y si esta en un punto que demora mucho?
    # alive_threads = threading.enumerate()
    # for t in alive_threads:
    #     if t.name == device.uid:
    #         print(f'>>>>> Thread {t.name} is still running')
    #         t.join()
    try:
        cstat = CorrStat(device.port)
        cstat.stop()
    except serial.serialutil.SerialException as e:
        print(e)
    finally:
        cstat = CorrStat(device.port)
        if cstat.stop():
            if experiment_status is not None:
                experiment_status.state = 'stopped'
                experiment_status.save()
            device.action = 'none'
            device.status = 'ready'
            device.experiment_id = 0
            device.save()
            device.refresh_from_db()
            device_ser = DeviceSerializer(device)
            return Response(device_ser.data)
        else:
            delete_unconnected_corrstat_devices()
            db.connection.close()
            # return None  # pailas
            return Response({"message": "Device is not responding"}, status=status.HTTP_408_REQUEST_TIMEOUT)


@api_view(http_method_names=['DELETE'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_clean(request):
    deleted_devices = hard_stop_all_devices()
    return Response({
        "message": "All clean",
        "deleted_devices": deleted_devices
    })


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def device_detect(request):
    detect_corrstat_device()
    devices = Device.objects.all()
    device_ser = DeviceSerializer(devices, many=True)
    return Response(device_ser.data)


@api_view(http_method_names=['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def export_experiment(request, pk):
    query_params = request.GET
    output_format = query_params['output_format']
    print('in export')
    print('output_format', output_format)
    http_response = HttpResponse()
    http_response['content_type'] = 'text/plain'
    http_response['Content-Disposition'] = 'attachment;'
    http_response.write(generate_experiment_export_file(pk, output_format))
    return http_response

@api_view(http_method_names=['GET'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def export_many_experiments(request):
    output_format = request.GET.get('output_format', 'multicic')
    experiments_to_export = request.GET.get('experiments', '')
    experiments_pks = [int(x) for x in experiments_to_export.split(',')]
    path = str(Path.home()) + "/temporal_zipfiles"
    Path(path).mkdir(parents=True, exist_ok=True)
    file_name = "experiments"
    file_path = f"{path}/zip/{file_name}.zip"
    generate_experiments_zip_file(experiments_pks, output_format, path, file_name)
    return FileResponse(open(file_path, 'rb'), as_attachment=True)


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
@parser_classes([ExperimentParser])
def import_experiment(request, format=None):
    parsed_data = request.data
    print('----------------------------------------------------')
    print(parsed_data)
    print('----------------------------------------------------')
    # Create Experiment
    parsed_data['Experiment']['technique_tag'] = Technique.objects.get(
        technique=parsed_data['Experiment']['technique_tag'])
    print(parsed_data['Experiment']['technique_tag'])

    experiment = Experiment.objects.create(
        user_id=request.user,
        **parsed_data['Experiment']
    )

    # Create the experiment config
    experiment_config = ExperimentConfig.objects.create(
        experiment_id=experiment,
        **parsed_data['ExperimentConfig']
    )

    # create experiment status
    experiment_status = ExperimentStatus.objects.create(
        experiment_id=experiment,
        **parsed_data['ExperimentStatus'])
    experiment_ser = ExperimentSerializer(experiment)
    # info = get_experiment_info(experiment)

    insert_imported_data_to_db(parsed_data['data'], experiment)

    # Create Thread
    # experiment_thread = threading.Thread(
    #     target=insert_imported_data_to_db,
    #     args=(parsed_data['data'], experiment),
    #     daemon=False,
    #     name="Import_" + str(experiment.pk)
    # )
    # print('~~~~~~~ Start Import Experiment Thread ~~~~~~~')
    # experiment_thread.start()

    return Response(experiment_ser.data)


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def system_shutdown_now(request):
    import subprocess
    device = Device.objects.filter(status='busy')
    if not device:
        subprocess.run(['sudo', 'shutdown', 'now'])
        return Response('Apagando')
    else:
        return Response({'detail': 'Hay un experimento en curso'}, status=status.HTTP_412_PRECONDITION_FAILED)


@api_view(http_method_names=['POST'])
@authentication_classes([SessionAuthentication, BasicAuthentication])
@permission_classes([IsAuthenticated])
def system_reboot(request):
    import subprocess
    device = Device.objects.filter(status='busy')
    if not device:
        subprocess.run(['sudo', 'reboot'])
        return Response('Reiniciando')
    else:
        return Response({'detail': 'Hay un experimento en curso'}, status=status.HTTP_412_PRECONDITION_FAILED)

