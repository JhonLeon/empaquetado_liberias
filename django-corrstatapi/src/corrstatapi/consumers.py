import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from .models import Data, ExperimentStatus
# from .serializers import ExperimentStatusSerializer
# from .scripts import format_experiment_data
# from channels.layers import get_channel_layer
# from asgiref.sync import async_to_sync


class CorrStatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['experiment_id']
        self.room_group_name = 'corrstat_%s' % self.room_name

        print("room group name: ", self.room_group_name)

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        # message = text_data_json['message']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            text_data_json
        )

    # Receive message from room group
    def corrstat_message(self, event):
        # message = event['message']

        print(" >> sending message to channel <<")
        # print(event)

        # Send message to WebSocket
        self.send(text_data=json.dumps(event))


# @receiver(post_save, sender=Data)
# def update_data(sender, instance, created, **kwargs):
#     if created:
#         experiment = instance.experiment_id
#         experiment_status = ExperimentStatus.objects.get(
#             experiment_id=experiment)

#         experiment_status_ser = ExperimentStatusSerializer(experiment_status)

#         message = {
#             "type": "corrstat_message",
#             "datapoint": format_experiment_data(instance, experiment.technique_tag.technique),
#             "experiment_status": experiment_status_ser.data,
#             "experiment_running": True
#         }
#         group = f"corrstat_{experiment.id}"

#         print(f"Send to {group}: ", message)

#         channel_layer = get_channel_layer()
#         async_to_sync(channel_layer.group_send)(
#             group,
#             message
#         )
