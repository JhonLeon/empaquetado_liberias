from rest_framework import serializers
from .models import Technique
from .models import Experiment
from .models import Device
from .models import ExperimentConfigStorage
from .models import ExperimentConfig
from .models import ExperimentStatus
from .models import Data
from .models import Material
from .models import ExperimentEqua

class TechniqueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Technique
        fields = "__all__"
        extra_kwargs = {'technique': {'required': True}, }


class ExperimentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experiment
        fields = "__all__"
        read_only_fields = [
            'date',
            'time',
            'user_id',
            'technique_tag',
        ]
        extra_kwargs = {'name': {'required': True},
                        'comment': {'required': False},
                        'date': {'required': False},
                        'time': {'required': False},
                        'user_id': {'required': True},
                        'technique_tag': {'required': True},
                        }


class ExperimentExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experiment
        exclude = ['id', 'user_id']


class ExperimentConfigSerializer(serializers.ModelSerializer):
    points_decade = serializers.ReadOnlyField()

    class Meta:
        model = ExperimentConfig
        fields = "__all__"


class ExperimentConfigExportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExperimentConfig
        exclude = ['id', 'experiment_id']

class ExperimentEquaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExperimentEqua
        fields = "__all__"

class ExperimentEquaExportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExperimentEqua
        exclude = ['id', 'experiment_id']

class ExperimentConfigStorageSerializer(serializers.ModelSerializer):
    points_decade = serializers.ReadOnlyField()

    class Meta:
        model = ExperimentConfigStorage
        fields = "__all__"


class ExperimentStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExperimentStatus
        fields = "__all__"
        extra_kwargs = {'state': {'required': False},
                        'actual_point': {'required': False},
                        'progress': {'required': False},
                        'timeleft': {'required': False},
                        'experiment_id': {'required': False},
                        }


class ExperimentStatusExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExperimentStatus
        exclude = ['id', 'experiment_id']


class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = "__all__"


class DataExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        exclude = ['id', 'experiment_id']


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = "__all__"
        extra_kwargs = {'idn': {'required': False},
                        'name': {'required': False},
                        'port': {'required': False},
                        'status': {'required': False},
                        'action': {'required': False},
                        'experiment_id': {'required': False},
                        }

class MaterialExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = "__all__"
        extra_kwargs = {'material': {'required': True},
                        'densidad': {'required': True},
                        'pesoEquivalente': {'required': True},
                        }