from corrstatlink import CorrStat, serial_ports
from corrstatlink import format_ac_data
from .models import Device, Experiment, ExperimentStatus, ExperimentConfig, Data, Technique,ExperimentEqua, Material
from django.utils import timezone
from django.utils.text import slugify
from django.contrib.auth.models import User
from django import db
from .exceptions import NotImplementedError
from .serializers import ExperimentSerializer
from .serializers import ExperimentConfigSerializer
from .serializers import ExperimentStatusSerializer
from .serializers import DeviceSerializer
from .serializers import ExperimentConfigExportSerializer, ExperimentExportSerializer, ExperimentStatusExportSerializer,ExperimentEquaSerializer,ExperimentEquaExportSerializer
from .serializers import DataExportSerializer
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.shortcuts import get_object_or_404
import impexp

import time
import math
import os
import shutil
from zipfile import ZipFile
from . import exceptions


def format_experiment_data(data, technique_tag):
    if technique_tag == 'EIS':
        return format_experiment_data_as_EIS(data)
    elif technique_tag == 'LPR':
        return format_experiment_data_as_LPR(data)
    elif technique_tag == 'CPP':
        return format_experiment_data_as_CPP(data)
    elif technique_tag == 'TAFEL':
        return format_experiment_data_as_TAFEL(data)
    raise NotImplementedError(f'Technique tag {technique_tag} not implemented')

def format_experiment_data_as_TAFEL(data):
    try:
        potential = [d.potential for d in data]
        current = [d.current for d in data]
        return {
            "tafel": list2chartjs(potential, current)
        }
    except:
        return {
            "tafel": list2chartjs([data.potential], [data.current])
        }


def format_experiment_data_as_LPR(data):
    try:
        potential = [d.potential for d in data]
        current = [d.current for d in data]
        return {
            "lpr": list2chartjs(potential, current)
        }
    except:
        return {
            "lpr": list2chartjs([data.potential], [data.current])
        }


def format_experiment_data_as_CPP(data):
    potential = [d.potential for d in data]
    current = [d.current for d in data]
    return {
        "cpp": list2chartjs(potential, current)
    }


def format_experiment_data_as_EIS(data):
    try:
        zreal = [d.zreal for d in data]
        zimag = [d.zimag for d in data]
        frequency = data.values_list('frequency', flat=True)
        phase = data.values_list('phase', flat=True)
        magnitude = data.values_list('magnitude', flat=True)

        return {
            'nyquist': list2chartjs(zreal, zimag),
            'bode_magnitude': list2chartjs(frequency, magnitude),
            'bode_phase': list2chartjs(frequency, phase)
        }
    except TypeError:
        zreal = data.zreal
        zimag = data.zimag
        frequency = data.frequency
        phase = data.phase
        magnitude = data.magnitude

        return {
            'nyquist': list2chartjs([zreal], [zimag]),
            'bode_magnitude': list2chartjs([frequency], [magnitude]),
            'bode_phase': list2chartjs([frequency], [phase])
        }


def get_experiment_info(experiment):

    if experiment.technique_tag.technique == "EIS":
        experiment_ser = ExperimentSerializer(experiment)
        experiment_config = ExperimentConfig.objects.get(
            experiment_id=experiment.id)
        experiment_config_ser = ExperimentConfigSerializer(experiment_config)
        experiment_status = ExperimentStatus.objects.get(
            experiment_id=experiment.id)
        experiment_status_ser = ExperimentStatusSerializer(experiment_status)

        data = Data.objects.filter(experiment_id=experiment.id)
        potential = list(data.values_list('potential', flat=True))
        current = list(data.values_list('current', flat=True))

        message = {
            'experiment': experiment_ser.data,
            'experiment_status': experiment_status_ser.data,
            'experiment_config': experiment_config_ser.data,
            'data': format_experiment_data(data, experiment.technique_tag.technique),
            'potential': potential,
            'current': current,

        }

        return message

    elif experiment.technique_tag.technique != "EIS":
        experiment_ser = ExperimentSerializer(experiment)
        experiment_config = ExperimentConfig.objects.get(
            experiment_id=experiment.id)
        experiment_config_ser = ExperimentConfigSerializer(experiment_config)
        experiment_status = ExperimentStatus.objects.get(
            experiment_id=experiment.id)
        experiment_status_ser = ExperimentStatusSerializer(experiment_status)

        experiment_equa = ExperimentEqua.objects.get(experiment_id=experiment.id)        
        experiment_config_eq = ExperimentEquaSerializer(experiment_equa)

        data = Data.objects.filter(experiment_id=experiment.id)
        potential = list(data.values_list('potential', flat=True))
        current = list(data.values_list('current', flat=True))

        message = {
            'experiment': experiment_ser.data,
            'experiment_status': experiment_status_ser.data,
            'experiment_config': experiment_config_ser.data,
            'ExperimentEqua': experiment_config_eq.data,
            'data': format_experiment_data(data, experiment.technique_tag.technique),
            'potential': potential,
            'current': current,
        }

        return message

def list2chartjs(x_data: list, y_data: list) -> list:
    data = []
    for x, y in zip(x_data, y_data):
        data.append({'x': x, 'y': y})
    return data


def points2points_decade(points, freq_max_hz, freq_min_hz):

    try:
        points_decade = int(points / (math.log10(freq_max_hz) -
                                      math.log10(freq_min_hz)))
    except ValueError:
        return 0
    return points_decade


def points_decade2points(points_decade, freq_max_hz, freq_min_hz):

    try:
        points = int(points_decade *
                     (math.log10(freq_max_hz) -
                      math.log10(freq_min_hz))) + 1
    except ValueError:
        return 0

    return points


def hard_stop_all_devices():
    devices = Device.objects.all()
    deleted_devices = []
    for device in devices:
        c = CorrStat(port=device.port)
        if not c.is_connected:
            device_ser = DeviceSerializer(device)
            deleted_devices.append(device_ser.data)
            device.delete()
            continue

        if c.stop(retries=10):
            device.status = 'ready'
            device.action = 'none'
            device.experiment_id = 0
            device.save()
        else:
            device_ser = DeviceSerializer(device)
            deleted_devices.append(device_ser.data)
            device.delete()
    return deleted_devices


def delete_unconnected_corrstat_devices():
    devices = Device.objects.all()
    for device in devices:
        if not device.status == 'busy':
            cstat = CorrStat(device.port)
            if not cstat.is_connected:
                device.delete()
            if cstat.is_ready:
                device.status = 'ready'
                device.action = 'none'
                device.experiment_id = 0
                device.save()


def detect_corrstat_device():
    delete_unconnected_corrstat_devices()
    ports = serial_ports()
    ports_in_db = list(Device.objects.all().values_list('port', flat=True))
    for port in ports:
        if port not in ports_in_db:
            cstat = CorrStat(port)
            cstat.stop(retries=10)
            # ensure not experiments are running
            if cstat.is_ready:
                uid = cstat.get_uid()
                device = Device(
                    name=f'{cstat.TAG_ID}-{uid}',
                    uid=uid,
                    idn=cstat.TAG_ID,
                    port=port,
                    experiment_id=0,
                    status='ready',
                    action='none')
                device.save()


def start_corrstat_experiment(device_uid, config, experiment, pause_timeout=7200):

    #check if config is valid for experiment 
    if (config['dc_type'] != 'Ninguna') and experiment.technique_tag.technique == "EIS":
        exit("Wrong config")

    if (config['dc_type'] == 'Ninguna') and experiment.technique_tag.technique != "EIS":
        exit("Wrong config")

    # get the device instance
    device = Device.objects.get(uid=device_uid)

    # Put busy state to avoid race conditions
    device.status = 'busy'
    device.save()

    device.refresh_from_db()
    # print('>>>> device status: ', device.status)

    # get channel layer for websocket messages
    channel_layer = get_channel_layer()

    # Check if hardware ready
    try:
        cstat = CorrStat(device.port)
        cstat.stop()
        cstat_ready = cstat.is_ready
    except Exception as e:
        cstat_ready = False
        print(e)

    if not cstat_ready:
        # return None
        device.status = 'off'
        device.experiment_id = 0
        device.save()
        message = {
            "type": "corrstat_message",
            "experiment_running": False
        }
        group = f"corrstat_{experiment.id}"
        async_to_sync(channel_layer.group_send)(
            group,
            message
        )
        experiment.delete()
        db.connection.close()
        raise exceptions.DeviceNotReadyError()

    # check if experiment is AC
    if config['dc_type'] == 'Ninguna':

        if 'points_decade' in config.keys():
            config['points'] = points_decade2points(
                config['points_decade'],
                config['freq_max_hz'],
                config['freq_min_hz']
            )
        # Create the experiment config
        experiment_config = ExperimentConfig.objects.create(
            freq_max_hz=config['freq_max_hz'],
            freq_min_hz=config['freq_min_hz'],
            amplitude_mv=config['amplitude_mv'],
            ocp=config['ocp'],
            sens=config['sens'],
            dc_type=config['dc_type'],
            # initial_potential_mv=config['start_potential_mv'],
            # final_potential_mv=config['end_potential_mv'],
            # scan_rate=config['scan_rate'],
            points=config['points'],
            experiment_id=experiment
        )

        # create experiment status
        experiment_status = ExperimentStatus.objects.create(
            experiment_id=experiment,
            state='running',
            timeleft='00:00:00')

        device.experiment_id = experiment.pk
        device.save()

        # Config hardware
        del config['dc_type']
        cstat.set_ac_config(config)
        cstat.send_ac_config()

    elif (config['dc_type'] == 'Lineal') or (config['dc_type'] == 'Cíclica'):
        # Create the experiment config
        experiment_config = ExperimentConfig.objects.create(
            dc_type=config['dc_type'],
            initial_potential_mv=config['initial_potential_mv'],
            final_potential_mv=config['final_potential_mv'],
            scan_rate=config['scan_rate'],
            points=config['points'],
            volt_ref=config['volt_ref'],
            experiment_id=experiment
        )

        # create experiment status
        experiment_status = ExperimentStatus.objects.create(
            experiment_id=experiment,
            state='running',
            timeleft='00:00:00')

        # create experiment equation
        material_equa = get_object_or_404(
        Material, material=config['material'])

        experiment_aqua = ExperimentEqua.objects.create(
            area=config['area'],
            beta=config['beta'],
            material=Material.objects.get(material=config['material']),
            densidad=material_equa.densidad,
            pesoEquivalente=material_equa.pesoEquivalente,
            experiment_id=experiment
        )     
        experiment_aqua.save()

        print("Ready to start DC EXPERIMENT!!")

        device.experiment_id = experiment.pk
        device.save()

        cstat.set_dc_config(experiment_config.get_dc_config())
        cstat.send_config(cstat.gen_dc_config)
    else:
        device.status = 'ready'
        device.save()
        db.connection.close()
        return exceptions.NotImplementedError()

    # Loop Measurement and get data
    import json
    print(f'|||||| Started ||||||')
    if experiment_config.isAC:
        for actual_point, corrstat_data in enumerate(cstat.read_ac_data(format_ac_data)):
            print(f'>>>>> Point {actual_point}')
            data_to_print = corrstat_data.copy()
            data_to_print.pop('voltage_signal')
            data_to_print.pop('current_signal')
            print(json.dumps(data_to_print, indent=4))
            d = Data.objects.create(
                experiment_id=experiment,
                frequency=corrstat_data['frequency'],
                magnitude=corrstat_data['magnitude'],
                phase=corrstat_data['phase'],
                potential=corrstat_data['ocp_potential_mV'],
                current=corrstat_data['dc_current_mA'],
                index=actual_point + 1
            )

            print(f'>>>>> Progress {corrstat_data["progress"]}')
            experiment_status.progress = corrstat_data['progress']
            experiment_status.timeleft = time.strftime(
                '%H:%M:%S', time.gmtime(corrstat_data['timeleft']))
            experiment_status.actual_point = actual_point + 1
            experiment_status.save()

            device.refresh_from_db()

            # ---- Send to Websocket ----
            x_dummy = list(range(len(corrstat_data['voltage_signal'])))
            corrstat_data['voltage_signal'] = list2chartjs(
                x_dummy, corrstat_data['voltage_signal'])
            corrstat_data['current_signal'] = list2chartjs(
                x_dummy, corrstat_data['current_signal'])
            experiment_status_ser = ExperimentStatusSerializer(experiment_status)
            message = {
                "type": "corrstat_message",
                "datapoint": format_experiment_data(d, experiment.technique_tag.technique),
                "experiment_status": experiment_status_ser.data,
                "experiment_running": True,
                'potential': d.potential,
                'current': d.current,
            }
            group = f"corrstat_{experiment.id}"
            print(f"Send to {group}: ", message)
            # print(json.dumps(message, indent=4))
            message.update({"corrstat_data": corrstat_data})

            async_to_sync(channel_layer.group_send)(
                group,
                message
            )

            if device.status == 'ready' or device.status == 'off' or device.status == 'stopping':
                db.connection.close()
                # return None
                exit("Status not busy")

            # Check if PAUSE
            if device.action == 'pause':
                print(f'|||||| Paused ||||||')
                experiment_status.state = 'paused'
                experiment_status.save()

                device.status = 'paused'
                device.save()
                timeout = time.time() + pause_timeout
                while device.action == 'pause':
                    time.sleep(30)
                    device.refresh_from_db()
                    print('Experimento pausado por el usuario')
                    timeleft = timeout - time.time()
                    message = {
                        "type": "corrstat_message",
                        "pause_timeleft": time.strftime('%H:%M:%S', time.gmtime(timeleft))
                    }
                    group = f"corrstat_{experiment.id}"
                    if timeleft < 0:
                        device.action = 'none'
                        device.status = 'ready'
                        device.save()
                        experiment_status.state = 'stopped'
                        experiment_status.save()
                        message['pause_timeleft'] = time.strftime(
                            '%H:%M:%S', time.gmtime(0))
                        async_to_sync(channel_layer.group_send)(
                            group,
                            message
                        )
                        db.connection.close()
                        raise exceptions.PauseTimeOutError()

                    async_to_sync(channel_layer.group_send)(
                        group,
                        message
                    )

                experiment_status.state = 'running'
                experiment_status.save()

                device.action = 'none'
                device.status = 'busy'
                device.save()

            # check if STOP
            if device.action == 'stop':
                print(f'|||||| Stoped ||||||')
                device.status = 'stopping'
                device.save()
                exit('Experiment Stoped by user')
    
    print(f"isDC: {experiment_config.isDC}")
    if experiment_config.isDC:
        print("#######################################################")
        print("~~~ INICIO EXPERIMENTO DE DC ~~~")
        print("#######################################################")
        
        for actual_point, corrstat_data in enumerate(cstat.read_dc_data()):
            print(json.dumps(corrstat_data, indent=4))
            if actual_point == 0:
                continue
            device.refresh_from_db()
            print(f'>>>>> Point {actual_point}')
            d = Data.objects.create(
                experiment_id=experiment,
                frequency=0,
                magnitude=0,
                phase=0,
                potential=corrstat_data['dc_potential_mV'],
                current=corrstat_data['dc_current_mA'],
                index=actual_point
            )

            print(f'>>>>> Progress {corrstat_data["progress"]}')
            experiment_status.progress = corrstat_data['progress']
            experiment_status.timeleft = time.strftime(
                '%H:%M:%S', time.gmtime(corrstat_data['timeleft']))
            experiment_status.actual_point = actual_point + 1
            experiment_status.save()

            # ---- Send to Websocket ----
            experiment_status_ser = ExperimentStatusSerializer(experiment_status)
            message = {
                "type": "corrstat_message",
                "datapoint": format_experiment_data(d, experiment.technique_tag.technique),
                "experiment_status": experiment_status_ser.data,
                "experiment_running": True,
                'potential': d.potential,
                'current': d.current,
            }
            group = f"corrstat_{experiment.id}"
            print(f"Send to {group}: ", message)
            # print(json.dumps(message, indent=4))
            message.update({"corrstat_data": corrstat_data})

            async_to_sync(channel_layer.group_send)(
                group,
                message
            )

            if device.status == 'ready' or device.status == 'off' or device.status == 'stopping':
                db.connection.close()
                # return None
                exit("Status not busy")
            # Check if PAUSE
            if device.action == 'pause':
                print(f'|||||| Paused ||||||')
                experiment_status.state = 'paused'
                experiment_status.save()

                device.status = 'paused'
                device.save()
                timeout = time.time() + pause_timeout
                while device.action == 'pause':
                    time.sleep(30)
                    device.refresh_from_db()
                    print('Experimento pausado por el usuario')
                    timeleft = timeout - time.time()
                    message = {
                        "type": "corrstat_message",
                        "pause_timeleft": time.strftime('%H:%M:%S', time.gmtime(timeleft))
                    }
                    group = f"corrstat_{experiment.id}"
                    if timeleft < 0:
                        device.action = 'none'
                        device.status = 'ready'
                        device.save()
                        experiment_status.state = 'stopped'
                        experiment_status.save()
                        message['pause_timeleft'] = time.strftime(
                            '%H:%M:%S', time.gmtime(0))
                        async_to_sync(channel_layer.group_send)(
                            group,
                            message
                        )
                        db.connection.close()
                        raise exceptions.PauseTimeOutError()

                    async_to_sync(channel_layer.group_send)(
                        group,
                        message
                    )

                experiment_status.state = 'running'
                experiment_status.save()

                device.action = 'none'
                device.status = 'busy'
                device.save()

            # check if STOP
            if device.action == 'stop':
                print(f'|||||| Stoped ||||||')
                device.status = 'stopping'
                device.save()
                exit('Experiment Stoped by user')

    experiment_status.state = 'finished'
    experiment_status.timeleft = '00:00:00'
    experiment_status.save()

    device.action = 'none'
    device.status = 'ready'
    device.experiment_id = 0
    device.save()
    print(f'|||||| Finished ||||||')

    db.connection.close()
    # return True
    # exit('Experiment Finished')


def insert_imported_data_to_db(datapoints, experiment):
    for datapoint in datapoints:
        Data.objects.create(
            experiment_id=experiment,
            **datapoint
        )
    # return experiment


def generate_experiment_export_file(pk, output_format):
    experiment = get_object_or_404(Experiment, pk=pk)
    print(experiment.technique_tag.technique)
    experiment_status = get_object_or_404(ExperimentStatus, experiment_id=experiment)
    experiment_config = get_object_or_404(ExperimentConfig, experiment_id=experiment)
    data = Data.objects.filter(experiment_id=experiment)

    experiment_ser = ExperimentExportSerializer(experiment)
    experiment_status_ser = ExperimentStatusExportSerializer(experiment_status)
    experiment_config_ser = ExperimentConfigExportSerializer(experiment_config)
    data_ser = DataExportSerializer(data, many=True)

    if experiment.technique_tag.technique == 'EIS':
        content = {
            "Experiment": experiment_ser.data,
            "ExperimentStatus": experiment_status_ser.data,
            "ExperimentConfig": experiment_config_ser.data,
            'Data': data_ser.data
        }
        print('--------> content')
        print(content)
        file_content = impexp.export_eis_datafile(
            content, file_format=output_format)
        return file_content

    elif experiment.technique_tag.technique != 'EIS':
        experiment_config_equation = get_object_or_404(ExperimentEqua, experiment_id=experiment)
        experiment_equation_ser = ExperimentEquaExportSerializer(experiment_config_equation)
        content = {
            "Experiment": experiment_ser.data,
            "ExperimentStatus": experiment_status_ser.data,
            "ExperimentConfig": experiment_config_ser.data,
            "ExperimentEqua": experiment_equation_ser.data,
            'Data': data_ser.data
        }
        print('--------> content')
        print(content)
        file_content = impexp.export_eis_datafile(
            content, file_format=output_format)
        return file_content

def get_all_file_paths(directory):
  
    # initializing empty file paths list
    file_paths = []
  
    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
  
    # returning all file paths
    return file_paths        

def generate_experiments_zip_file(pks, output_format, path, filename):
    # Create Temp Dir}
    if os.path.exists(path):
        shutil.rmtree(path)
        os.mkdir(path)
    os.mkdir(f"{path}/dir")
    os.mkdir(f"{path}/zip")
    for pk in pks:
        try:
            experiment = get_object_or_404(Experiment, pk=pk)
            f = open(f"{path}/dir/{slugify(experiment.name)}_{experiment.id}.{output_format}", "+w")
            file_content = generate_experiment_export_file(pk, output_format)
            f.write(file_content)
            f.close()
        except:
            f.write("")
            f.close()
    # shutil.make_archive(f"{path}/zip/{filename}", 'zip', f"{path}/zip")
    # writing files to a zipfile
    file_paths = get_all_file_paths(f"{path}/dir")
    with ZipFile(f"{path}/zip/{filename}.zip",'w') as zipf:
        # writing each file one by one
        for f in file_paths:
            zipf.write(f,arcname=os.path.basename(f))
    return f"{path}/zip"
