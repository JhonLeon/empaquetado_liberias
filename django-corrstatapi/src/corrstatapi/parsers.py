from rest_framework.parsers import BaseParser
from impexp import import_eis_datafile


class ExperimentParser(BaseParser):
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        print('iside parser')
        query_params = parser_context['request'].GET

        experiment_name = 'imported_experiment'
        if 'experiment_name' in query_params.keys():
            experiment_name = query_params['experiment_name']

        file_format = None
        if 'file_format' in query_params.keys():
            file_format = query_params['file_format']

        experiment_comment = ''
        if 'experiment_comment' in query_params.keys():
            experiment_comment = query_params['experiment_comment']

        output_format = query_params['output_format']
        technique = query_params['technique']

        data = stream.read().decode('utf-8', errors='ignore')
        if technique == 'EIS':
            data = import_eis_datafile(
                data,
                output_format=output_format,
                experiment_name=experiment_name,
                file_format=file_format,
                experiment_comment=experiment_comment
            )

        return data
