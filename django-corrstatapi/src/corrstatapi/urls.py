from django.urls import path
from .views import DeviceList
from .views import DeviceDetail
from .views import device_pause
from .views import device_start
from .views import device_stop
from .views import device_detect
from .views import device_resume
from .views import ExperimentStatusListView
from .views import ExperimentStatusDetailView
from .views import TechniqueListView
from .views import ExperimentListView
from .views import ExperimentDetailView
from .views import ExperimentConfigDetailView
from .views import ExperimentConfigListView
from .views import ExperimentConfigStorageDetailView
from .views import ExperimentConfigStorageListView
from .views import experiment_info_view
from .views import add_experiment_config_to_storage
from .views import import_experiment, export_experiment
from .views import device_clean
from .views import retrive_experiment_device
from .views import system_reboot
from .views import system_shutdown_now
from .views import export_many_experiments
from .views import ListMaterial
app_name = 'corrstatapi'

urlpatterns = [
    # Device
    path('device/', device_clean, name="cleandevice"),
    path('device/list/', DeviceList.as_view(), name="listdevice"),
    path('device/detail/<slug:pk>/', DeviceDetail.as_view(), name="detaildevice"),
    path('device/start/<slug:pk>/', device_start, name="startdevice"),
    path('device/stop/<slug:pk>/', device_stop, name="stopdevice"),
    path('device/pause/<slug:pk>/', device_pause, name="pausedevice"),
    path('device/resume/<slug:pk>/', device_resume, name="resumedevice"),
    path('device/detect/', device_detect, name="detectdevice"),
    # Experiment Status
    path('experiment_status/list/', ExperimentStatusListView.as_view(),
         name="list_experimentstatus"),
    path('experiment_status/detail/<int:pk>/', ExperimentStatusDetailView.as_view(),
         name="detail_experimentstatus"),
    # Techniques
    path('techniques/list/', TechniqueListView.as_view(), name="listtechnique"),
    # Experiment
    path('experiment/list/', ExperimentListView.as_view(), name="listexperiment"),
    path('experiment/detail/<int:pk>/', ExperimentDetailView.as_view(),
         name="detailexperiment"),
    path('experiment/info/<int:pk>/',
         experiment_info_view, name="experiment_info"),
    path('experiment/import/', import_experiment, name='import_experiment'),
    path('experiment/export/',
         export_many_experiments, name='export_experiment'),
    path('experiment/export/<int:pk>/',
         export_experiment, name='export_experiment'),
    path('experiment/device/<int:pk>/',
         retrive_experiment_device, name='retrive_experiment_device'),
    # Experiment Config
    path('experiment_config/list/', ExperimentConfigListView.as_view(),
         name="list_experiment_config"),
    path('experiment_config/detail/<int:pk>/', ExperimentConfigDetailView.as_view(),
         name="detail_experiment_config"),
    # Experiment Config Storage
    path('experiment_config_storage/list/', ExperimentConfigStorageListView.as_view(),
         name="list_experiment_config_storage"),
    path('experiment_config_storage/detail/<int:pk>/', ExperimentConfigStorageDetailView.as_view(),
         name="detail_experiment_config_storage"),
    path('save_experiment_config/<int:experiment_id>/',
         add_experiment_config_to_storage, name="add_experiment_storage"),
    path('system/shutdown_now/', system_shutdown_now, name='shutdown_now'),
    path('system/reboot/', system_reboot, name='reboot'),
     #Llamado a la lista de material
    path('list/material/', ListMaterial.as_view(), name="list_material"),


]
