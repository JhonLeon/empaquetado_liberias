CORRSTAT API
============

corrstatapi contiene modelos y vistas que permiten correr pruebas en un
dispositivo corrstat.


Instalación
-----------

1. Agregue "corrstatapi" a INSTALLED_APPS::

    INSTALLED_APPS = [
    ...
    'corrstatapi',
    ]

2. Incluya las urls de corrstat api en el archivo urls.py del proyecto.

    path('api/', include('corrstatapi.urls'))

3. Corra el comando `python manage.py migrate` para crear los modelos de corrstatapi.

4. Inicie el servidor con `python manage.py runserver` y explore los modelos desde
la pagina '/admin'

5. Listo, revise el archivo de postman para hacer pruebas con la API.


Configuración de Channels
-------------------------

Para configurar los mesajes de django-channels

1. Agregue lo sigueinte a settings.py
```
ASGI_APPLICATION = 'main.asgi.application'
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}
```

2. agregue lo siguiente en asgi.py:
```
import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
import corrstatapi.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": AuthMiddlewareStack(
        URLRouter(
            corrstatapi.routing.websocket_urlpatterns
        )
    ),
})
```

Tenga en cuenta que en este caso `main` es el nombre de la aplicación principal de django.

3. Levante un servidor de redis:
```
docker run -p 6379:6379 -d redis:5
```