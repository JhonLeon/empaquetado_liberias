import yaml
import pandas as pd
import io
import numpy as np
import datetime
import json

PKG_NAME = 'CIC-impexp'
SEPARATOR = "\n" + '-'*80 + "\n"


def export_complete(ser_data: dict, file_name: str = None):
    """
    Toma un dict con la información a exportar y retorna un string con formato compatible con multicic
    :param ser_data: Diccionario con los datos a exportar, debe tener la siguiente estructura:
        {   "Experiment": <Objeto serializado del modelo Experiment>
            "ExperimentStatus": <Objeto serializado del modelo ExperimentStatus>
            "ExperimentConfig": <Objeto serializado del modelo ExperimentConfig>
            'Data': <Lista de objetos serializados del modelo Data>
        }
    :param file_name: Opcional. Ruta del archivo donde se quiere guardar la exportación.
    :return: Retorna un string con los datos en formato compatible con zview
    """
    # Write variables in YAML format
    header = [
        "# ARCHIVO DE DATOS DE MULTICIC",
        "# Desarrollado por la Corporación para la Investigación de la Corrosión - (CIC)",
        "# electronica1@corrosioncic.com",
        "# {} MultiCIC \n".format(PKG_NAME),
    ]
    units = {
        'frequency': 'Hz',
        'magnitude': 'Ohm',
        'phase': 'degree',
        'Zreal': 'Ohm',
        'Zimag': 'Ohm',
        'potential': 'mV',
        'current': 'mA'
    }
    s_data = dict(ser_data)
    data = s_data.pop('Data')
    s_data['units'] = units
    models_info = json.dumps(s_data, indent=4)

    # "\n".join(header) + models_info
    formatted_str = "{}{}{}{}".format(
        '\n'.join(header), SEPARATOR, models_info, SEPARATOR)
    # Convert DataFrame to CSV string
    df = pd.DataFrame.from_dict(data)
    clpx = np.array(df['magnitude'] * (np.cos(np.deg2rad(df['phase'])
                                              ) + np.sin(np.deg2rad(df['phase'])) * 1j))
    df['Zreal'] = clpx.real
    df['Zimag'] = clpx.imag
    # csv_str = df.to_csv(index=False, sep='\t', float_format='%7.17f')
    columns = ['frequency', 'magnitude', 'phase',
               'Zreal', 'Zimag', 'potential', 'current']
    # csv_str = df.to_string(float_format='%07.17f',
    #                        index=False, justify='center', columns=columns)

    df_dict = df.to_dict(orient='list')
    csv_str = json.dumps(df_dict, indent=4)

    tecnica = s_data['Experiment']['technique_tag']
    if tecnica != 'EIS':
        columns = ['potential','current']
        df1 = pd.DataFrame(df, columns=columns)
        df_dict = df1.to_dict(orient='list')
        csv_str = json.dumps(df_dict, indent=4)

    # Add CSV data into the str
    formatted_str += csv_str
    # Write the str to a file
    if file_name:
        with open(file_name, 'w') as file:
            file.write(formatted_str)
    else:
        return formatted_str


def export_zview_compatible(ser_data: dict, file_name: str = None):
    """
    Toma un dict con la información a exportar y retorna un string con formato compatible con zview
    :param ser_data: Diccionario con los datos a exportar, debe tener la siguiente estructura:
        {'Data': [{
                'frequency': float,
                'magnitude': float,
                'phase': float,
                }, ... ]
        }
    :param file_name: Opcional. Ruta del archivo donde se quiere guardar la exportación.
    :return: Retorna un string con los datos en formato compatible con zview
    """
    try:
        s_data = dict(ser_data)

        data = s_data.pop('Data')
        df = pd.DataFrame.from_dict(data)
        clpx = np.array(df['magnitude'] *
                        (np.cos(np.deg2rad(df['phase'])) + (np.sin(np.deg2rad(df['phase'])) * 1j)))
        df['Zreal'] = clpx.real
        df['Zimag'] = clpx.imag
        columns = ['frequency', 'Zreal', 'Zimag']
        formatted_str = df.to_csv(sep=',', index=False,
                                  header=False, columns=columns)
        #print (f"Esto es tag: {s_data['Experiment']['technique_tag']}")
        tecnica=s_data['Experiment']['technique_tag']
        if tecnica != "EIS":
            columns = ['potential', 'current']
            formatted_str = df.to_csv(sep=',', index=False,
                                  header=False, columns=columns)
    except:
        print("*"*80)
        print(
            f"Error exportando datos en la prueba: {s_data['Experiment']['name']}")
        formatted_str = ""
        pass

    if file_name:
        with open(file_name, 'w+') as file:
            file.write(formatted_str)
    else:
        return formatted_str


def import_zview_compatible(file_content):
    df = pd.read_csv(
        io.StringIO(file_content),
        sep=',',
        header=0,
        names=['frequency', 'Zreal', 'Zimag']
    )
    return df


def import_complete(file_content: str):
    _, config_section, data_section = file_content.split(SEPARATOR)
    cfg = json.loads(config_section)
    # df = pd.read_csv(io.StringIO(data_section), sep='\s+')
    df = pd.DataFrame.from_dict(json.loads(data_section))

    return cfg, df


def _get_technique(file_content: str, tokens: dict):
    """
    look for tokens in file_content string and return the technique associated with token found
    :param file_content: string with content of file
    :param tokens: dict of tokens and their related techniques.
    :return: str technique
    """
    for token in tokens.keys():
        if token in file_content:
            return tokens[token]
    return False


_TECHNIQUES = ['eis', 'lpr', 'tafel', 'ppc']
_IVIUM_TECH_TOKENS = {
    'Method=Impedance': 'eis',
    'Technique=Polarization Resistance': 'lpr',
    'Technique=Tafel': 'tafel',
    'Technique=PPC': 'ppc'  # Todo: confirmar el token real
}
_IVIUM_HEADERS_NAMES = {
    'eis': ['Zreal', 'Zimag', 'Frequency'],
    'lpr': ['Voltage', 'Current', '-'],
    'tafel': ['Voltage', 'Current', '-'],
    'ppc': ['Voltage', 'Current', '-']
}
_GAMRY_TECH_TOKENS = {
    'TAG	EISPOT': 'eis',
    'TAG	POLRES': 'lpr',
    'TAG	TAFEL': 'tafel',
    'TAG	CYCPOL': 'ppc',
    'TAG    POTENTIODYNAMIC': 'tafel'
}
_GAMRY_HEADERS_NAMES = {
    'eis': ['Pt', 'Time', 'frequency', 'Zreal', 'Zimag', 'Zsig', 'Zmod', 'Zphz', 'Idc', 'Vdc', 'IERange'],
    'lpr': ['Pt', 'T', 'Vf', 'Im', 'Vu', 'Sig', 'Ach', 'IERange', 'Over'],
    'tafel': ['Pt', 'T', 'Vf', 'Im', 'Vu', 'Sig', 'Ach', 'IERange', 'Over'],
    'ppc': ['Pt', 'T', 'Vf', 'Im', 'Vu', 'Sig', 'Ach', 'IERange', 'Over']
}
_GAMRY_TOKEN_DATA_TABLE = {
    'eis': 'ZCURVE	TABLE',
    'lpr': '\nCURVE	TABLE',
    'tafel': '\nCURVE	TABLE',
    'ppc': '\nCURVE	TABLE'
}
_IM6_TOKEN_FILE1 = 'File............:'
_IM6_TOKEN_FILE2 = 'File.......:'
_IM6_HEADERS_NAMES = {
    'eis': ['Number', 'frequency', 'Zreal', 'Zimag'],
    'lpr': ['Number', 'Time/s', 'Potential/V', 'Current/A'],
    'tafel': ['Number', 'Time/s', 'Potential/V', 'Current/A'],
    'ppc': ['Number', 'Time/s', 'Potential/V', 'Current/A']
}
_IM6_NROWS_TOKEN = 'Lines:'
_ZVIEW_HEADERS_NAMES = {
    'eis': ["Pt.", "frequency", "Zreal", "Zimag", "Frequency (Hz)", "|Z|", "theta"]
}
_ZVIEW_TITLE_TOKEN = "ZView Export Data File: Version 2.90"
_ZVIEW_TOKEN_DATA = 'theta'


def _get_ivium_technique(file_content: str):
    return _get_technique(file_content, _IVIUM_TECH_TOKENS)


def import_ivium_data(file_content: str):
    # Identify the Echem Technique
    header_names = _IVIUM_HEADERS_NAMES[_get_ivium_technique(file_content)]
    # Search for data, left the remaining as comments
    sections_str = file_content.split('primary_data')
    config = sections_str[0].encode("ascii", errors="ignore").decode()
    # Separate string by line endings to fix the format
    primary_data = sections_str[1].splitlines()
    # Free some RAM
    del sections_str
    # Initialize variable to store fixed csv string
    csv_data = []
    for line in primary_data:
        line = line.split()
        if line:
            csv_data.append(','.join(line))
    # Free some RAM
    del primary_data
    # Get the exact number of rows to parse
    # ncols = csv_data[0]
    nrows = int(csv_data[1])
    # Reshape the list to get only the data
    csv_data = csv_data[2:]
    csv_data_stream = io.StringIO('\n'.join(csv_data[0:nrows]))
    del csv_data

    # Parse the CSV and read as pandas DataFrame
    df = pd.read_csv(csv_data_stream, names=header_names, index_col=False)

    return config, df


def _get_gamry_technique(file_content: str):
    return _get_technique(file_content, _GAMRY_TECH_TOKENS)


def import_gamry_data(file_content: str):
    technique = _get_gamry_technique(file_content)

    # Identify the Echem Technique
    header_names = _GAMRY_HEADERS_NAMES[technique]
    # Search for data, left the remaining as comments
    sections_str = file_content.split(_GAMRY_TOKEN_DATA_TABLE[technique])
    # Separate string by line endings to fix the format
    primary_data = sections_str[1].splitlines()
    config = "Archivo Importado"  # sections_str[0]
    # Free some RAM
    del sections_str
    # Initialize variable to store fixed csv string
    csv_data = []
    for line in primary_data:
        line = line.replace(',', '.').split()
        if line:
            csv_data.append(';'.join(line))
    # Free some RAM
    del primary_data

    # Reshape the list to get only the data
    if technique != 'eis':
        csv_data = csv_data[3:]
    else:
        csv_data = csv_data[2:]
    # Convert into StringIO
    csv_data_stream = io.StringIO('\n'.join(csv_data))
    del csv_data
    # Parse the CSV and read as pandas DataFrame
    df = pd.read_csv(csv_data_stream, names=header_names,
                     index_col=False, sep=';')

    # print(type(df[['Pt', 'T']]))

    return config, df


def get_im6_test_names(file_content: str):
    first_line = file_content.split('\n')[0]
    if _IM6_TOKEN_FILE1 in first_line:
        IM6_TOKEN_FILE = _IM6_TOKEN_FILE1
    elif _IM6_TOKEN_FILE2 in first_line:
        IM6_TOKEN_FILE = _IM6_TOKEN_FILE2
    else:
        return []

    file_sections = file_content.split(IM6_TOKEN_FILE)
    name_list = []
    for section in file_sections[1:]:
        name_list.append(section.split('\n')[0])
    return name_list


def _get_im6_technique(test_name: str):
    if 'eis' in test_name.lower():
        return 'eis'
    if 'tfl' in test_name.lower():
        return 'tafel'
    if 'tafel' in test_name.lower():
        return 'tafel'
    if 'lpr' in test_name.lower():
        return 'lpr'
    if 'ppc' in test_name.lower():
        return 'ppc'
    return None


def import_im6_data(file_content: str, test_name: str = None):
    file_sections = file_content.split(test_name)
    test_str = file_sections[1]
    del file_sections
    test_str = test_str.split('Measure samples:')
    config_str = test_str[0]
    test_lines = test_str[1].split('\n')
    nrows = None
    start_index = None
    for idx, line in zip(range(len(test_lines)), test_lines):
        if _IM6_NROWS_TOKEN in line:
            nrows = int(line.split()[1])
            start_index = idx + 4
        if nrows is not None:
            test_lines[idx] = ';'.join(line.split())
            if idx > (start_index + nrows):
                break
    del test_str
    header_names = _IM6_HEADERS_NAMES[_get_im6_technique(test_name)]
    test_stream = io.StringIO(
        '\n'.join(test_lines[start_index:start_index + nrows]))
    df = pd.read_csv(test_stream, names=header_names, index_col=False, sep=';')

    return config_str, df


def import_zview_data(file_content: str):
    file_str = file_content.replace('\"', '')
    sections_str = file_str.split(_ZVIEW_TOKEN_DATA)
    config = sections_str[0]
    primary_data = sections_str[1].replace(',', '.').splitlines()
    # Free some RAM
    del sections_str
    del file_str
    # Initialize variable to store fixed csv string
    csv_data = []
    for line in primary_data:
        line = line.split()
        if line:
            csv_data.append(';'.join(line))
    # Free some RAM
    del primary_data
    csv_data_stream = io.StringIO('\n'.join(csv_data))
    del csv_data
    header_names = _ZVIEW_HEADERS_NAMES['eis']
    # Parse the CSV and read as pandas DataFrame
    df = pd.read_csv(csv_data_stream, names=header_names,
                     index_col=False, sep=';')

    return config, df


def get_file_format(file_content: str) -> str or None:
    """
    lee un archivo de texto y retorna qué formato tiene
    :param file_content: contenido del archivo str
    :return: nombre del formato 'gamry', 'ivium', 'im6' o None si no logra identificarlo
    """
    if _get_gamry_technique(file_content):
        return 'gamry'
    elif _get_ivium_technique(file_content):
        return 'ivium'
    elif _IM6_TOKEN_FILE1 in file_content or _IM6_TOKEN_FILE2 in file_content:
        return 'im6'
    elif _ZVIEW_TITLE_TOKEN in file_content:
        return 'zview'
    elif PKG_NAME in file_content:
        return 'multicic'
    else:
        return 'zview_compatible'


def find_im6_data_by_technique(file_content: str, technique: str) -> list:
    """
    busca en los títulos de las pruebas una que tenga la palabra deseada
    :param file_content: string con el contenido del archivo
    :param technique: palabra a buscar
    :return: lista de nombres que contienen la palabra
    """
    test_names = get_im6_test_names(file_content)
    ret = []
    for test_name in test_names:
        if _get_im6_technique(test_name) == technique:
            ret.append(test_name)
    return ret


def import_eis_datafile(file_content: str,
                        output_format: str = 'polar_degree',
                        file_format: str = None,
                        experiment_name: str = None,
                        experiment_comment: str = ''):

    config = {
        "ExperimentConfig": {
            "freq_max_hz": 0,
            "freq_min_hz": 0,
            "amplitude_mv": 0,
            "ocp": "Dinámico",
            "sens": "Rápido",
            "dc_type": "Ninguna",
            "start_potential_mv": 0,
            "end_potential_mv": 0,
            "scan_rate": 0,
            "points": 0
        },
        "ExperimentStatus": {
            "state": "imported",
            "actual_point": 0,
            "progress": 100,
            "timeleft": "00:00:00",
        },
        "Experiment": {
            "name": experiment_name,
            # "date": datetime.datetime.now().strftime("%d-%m-%Y"),
            # "time": datetime.datetime.now().strftime("%H:%M:%S"),
            "technique_tag": "EIS"
        }
    }

    if not experiment_name:
        experiment_name = "experimento importado"

    comment = None
    # Identify the file format
    if file_format is None:
        file_format = get_file_format(file_content)
    # Interpret the file according to the format
    if file_format == 'gamry':
        comment, df = import_gamry_data(file_content)
    elif file_format == 'ivium':
        comment, df, = import_ivium_data(file_content)
    elif file_format == 'im6':
        if experiment_name is None:
            test_names = find_im6_data_by_technique(file_content, 'eis')
            experiment_name = test_names[0]
        comment, df = import_im6_data(file_content, experiment_name)
    elif file_format == 'zview':
        comment, df = import_zview_data(file_content)
    elif file_format == 'multicic':
        config, df = import_complete(file_content)
        comment = config["Experiment"]["comment"]
        experiment_name = config["Experiment"]["name"]
    elif file_format == 'zview_compatible':
        df = import_zview_compatible(file_content)
        comment = ""
    else:
        config = {}
        df = None
        comment = None
    # Process the dataframe to unify all sources of data
    frequency = np.array(df['frequency'])
    zreal = np.array(df['Zreal'])
    zimag = np.array(df['Zimag'])
    
    if file_format == 'zview_compatible':
        current = [0 for i in range(len(frequency))]
        potential = [0 for i in range(len(frequency))]
    else: 
        current = np.array(df['current'])
        potential = np.array(df['potential'])


    z = zreal + zimag * 1j
    phase = np.angle(z)
    magnitude = np.abs(z)
    # Update some known fields
    # config['ExperimentConfig']['freq_max_hz'] = max(frequency)
    # config['ExperimentConfig']['freq_min_hz'] = min(frequency)
    # config['ExperimentConfig']['points'] = len(frequency)

    # Create the dict to return, and start adding information
    ret = {
        'format': file_format
    }
    # If no points add the length of the data as the number of points
    if not 'points' in config["ExperimentConfig"].keys():
        config["ExperimentConfig"]["points"] = len(frequency)

    if comment is not None:
        config["Experiment"]["comment"] = experiment_comment + \
            '\r\n' + comment
    else:
        config["Experiment"]["comment"] = experiment_comment

    if experiment_name is not None:
        config["Experiment"]["name"] = experiment_name

    # Add the config key to the return dict
    ret.update(config)
    # Process output data-format according to given arguments
    if output_format == 'polar':
        ret.update({'frequency': list(frequency),
                    'magnitude': list(magnitude),
                    'phase': list(phase),
                    })
    elif output_format == 'polar_degree':
        ret.update({'frequency': list(frequency),
                    'magnitude': list(magnitude),
                    'phase': list(np.rad2deg(phase)),
                    })
    elif output_format == 'rectangular':
        ret.update({'frequency': list(frequency),
                    'zreal': list(zreal),
                    'zimag': list(zimag),
                    })
    elif output_format == 'model_serializer':
        data = []
        for index in range(len(list(frequency))):
            data.append({
                'magnitude': magnitude[index],
                'frequency': frequency[index],
                'potential': potential[index],  # TODO Agregado por nestor
                'current': current[index],  # TODO
                'phase': np.rad2deg(phase[index]),
                'index': index,
            })
        ret.update({'data': data})
    return ret


def export_eis_datafile(ser_data: dict, file_name: str = None,
                        file_format: str = 'multicic'):
    """
    Toma un dict con información de experimento eis y genera un string con el formato deseado.
    :param ser_data: dict con la información eis
        {   "Experiment": <Objeto serializado del modelo Experiment>
            "ExperimentStatus": <Objeto serializado del modelo ExperimentStatus>
            "ExperimentConfig": <Objeto serializado del modelo ExperimentConfig>
            'Data': <Lista de objetos serializados del modelo Data>
        }
    :param file_name: Opcional. Nombre del archivo a gruardar
    :param file_format: formato a utilizar: 'multicic' o 'zview'
    :return: String de los datos con el formato seleccionado
    """
    if file_format == 'zview':
        return export_zview_compatible(ser_data, file_name)
    else:
        return export_complete(ser_data, file_name)


if __name__ == "__main__":
    passSEPARATOR = "\n" + '-'*80 + "\n"

