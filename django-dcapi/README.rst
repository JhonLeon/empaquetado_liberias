============
CORRSTAT API
============

dcapi contiene los algoritmos para analizar las tecnicas LPR y TAFEL.


Instalación
-----------

1. Agregue "dcapi" a INSTALLED_APPS::

	INSTALLED_APPS = [
	...
	'dcapi',
	]

2. Incluya las urls de corrstat api en el archivo urls.py del proyecto.

	path('api/dc/', include('dcapi.urls'))

3. Listo, revise el archivo de postman para hacer pruebas con la API.


Analisis de LPR:
----------------

Entrypoint: /api/dc/lpr/
Method: POST
Body:
```
{
    "current": [...],
    "potential": [...]
}
```


Analisis de TAFEL:
----------------

Entrypoint: /api/dc/tafel/
Method: POST
Body:
```
{
    "current": [...],
    "potential": [...]
}
```