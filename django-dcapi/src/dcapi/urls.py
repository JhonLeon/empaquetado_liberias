from django.urls import path
from . import views

app_name = 'dcapi'

urlpatterns = [
    path('lpr/', views.lpr, name="lpr"),
    path('tafel/', views.tafel, name="tafel"),
]
