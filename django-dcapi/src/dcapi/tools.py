
import numpy as np


def correlation(vector1, vector2):
    vector1 = np.array(vector1)
    vector2 = np.array(vector2)

    # proceso desviacion de la media vector1
    mean1 = np.median(vector1)
    vec1 = []
    for i in vector1:
        vec1.append(i-mean1)
    vec1 = np.array(vec1)
    # proceso desviacion de la media vector1
    mean2 = np.median(vector2)
    vec2 = []
    for i in vector2:
        vec2.append(i-mean2)
    vec2 = np.array(vec2)

    # multiplicacion de la desviacion de la media
    vec12 = vec1*vec2

    # cuadrado de la desviacion media
    vec1_2 = vec1**2

    # cuadrado de la desviacion media
    vec2_2 = vec2**2

    # print "Correlation: " + str(vec12.sum()/np.sqrt(vec1_2.sum()*vec2_2.sum()))

    return vec12.sum()/np.sqrt(vec1_2.sum()*vec2_2.sum())


def regression(x, y):
    x = np.array(x)
    y = np.array(y)

    # calculo de la media de cada set de datos
    Mx = np.median(x)  # Abscisa
    My = np.median(y)  # ordenada

    # calculo de las desviaciones estandar
    sx = x.std()
    sy = y.std()

    # calculo de la correlacion de pearson
    r = correlation(x, y)

    # Calculo ecuacion de la recta que mejor se aproxima a los puntos (best fit)
    m = r*(sy/sx)  # pendiente
    b = My-m*Mx

    return [m, b, r]


def smoothOutliers(current, nstd=1.5):
    dcurrent = np.diff(current)
    idxOutliers, = np.where(-dcurrent > nstd*np.std(dcurrent))

    if len(idxOutliers) == 0:
        # print "No outliers found"
        return 0

    smooth_current = np.array(current)

    for i in idxOutliers:
        if i == 1:
            smooth_current[i] = smooth_current[i+1]
        elif i == len(smooth_current):
            smooth_current[i] = smooth_current[i-1]
        else:
            # print self.current[i]
            smooth_current[i] = np.mean(
                np.array([[smooth_current[i-1]], [smooth_current[i+1]]]))
            # print self.current[i]
    # print self.current


# Calcular Resistencia de polarizacion

def computePolRes(current, potential, max_iters=1000, linear_adjustment_criteria=99, min_number_of_points=6):
    current = np.array(current)
    potential = np.array(potential)
    
    # encuentro index zero
    zero_val = abs(current).min()
    zero_idx, = np.where(abs(current) == zero_val)
    zero_idx = zero_idx[0]

    # incializo la ventana
    lpr_start_idx = int(0)
    lpr_end_idx = len(current)+1

    # separo la data en dos vectores, uno de valores antes de cero y otro
    # devalores despues de cero
    win_potential = potential[lpr_start_idx:lpr_end_idx]
    win_current = current[lpr_start_idx:lpr_end_idx]

    # print win_potential
    # print win_current

    neg_values = len(win_current[lpr_start_idx:zero_idx])
    pos_values = len(win_current[zero_idx:lpr_end_idx])

    total_neg_values = len(win_current[lpr_start_idx:zero_idx])
    total_pos_values = len(win_current[zero_idx:lpr_end_idx])

    majorSide = (pos_values > neg_values)
    # print "Neg Vals : ", neg_values
    # print "Pos Vals :", pos_values

    for i in range(0, max_iters-1):
        [m, b, r] = regression(win_potential, win_current)
        r2a = r*r*100

        if r2a > linear_adjustment_criteria:
            break
        else:
            if neg_values <= min_number_of_points and pos_values <= min_number_of_points:
                break

            if neg_values > min_number_of_points:
                lpr_start_idx = lpr_start_idx + 1

            if pos_values > min_number_of_points:
                lpr_end_idx = lpr_end_idx - 1

            win_potential = potential[lpr_start_idx:lpr_end_idx]
            win_current = current[lpr_start_idx:lpr_end_idx]

            neg_values = len(potential[lpr_start_idx:zero_idx])
            pos_values = len(potential[zero_idx:lpr_end_idx])

    print("ajuste inicial: ", r2a)
    # print "major side    : ", majorSide

    if r2a < linear_adjustment_criteria:
        print("Ultra Boost")
        if majorSide:
            niter = int(abs(total_pos_values-lpr_end_idx))
        else:
            niter = int(abs(total_neg_values-lpr_start_idx))

        for i in range(0, niter):
            if majorSide and ((lpr_end_idx + 1) < len(potential)):
                lpr_end_idx = lpr_end_idx + 1
            elif not majorSide and ((lpr_start_idx - 1) > 1):
                lpr_start_idx = lpr_start_idx - 1
            else:
                print("majorBreak")
                break
            # print "iter : ", i
            win_potential = potential[lpr_start_idx:lpr_end_idx]
            win_current = current[lpr_start_idx:lpr_end_idx]

            [m, b, r] = regression(win_potential, win_current)
            r2a = r*r*100

            if r2a > linear_adjustment_criteria:
                break

    # update results.
    Rm = m
    Rb = b
    R = 1/m

    print("Ajuste :", r2a)
    try:
        print("iteraciones : ", niter)
    except:
        print("no iter")
    # print m, b, r2a

    return {
        "R": R,
        "r2": r2a,
        "potential_points": list(win_potential),
        "current_points": list(win_current),
        "linear_potential": list(potential),
        "linear_current": list((potential*Rm) + Rb)
    }


def evaluate_linear(m, b, x):
    return (m*x)+b


def slopeChange(x, y, nump):
    step = np.floor(len(x)/nump)
    cnt = 1
    length = len(x)
    m_list = []

    # print "len x: ", len(x), " len y: ", len(y)

    for k in range(0, length+1, int(step)):
        end_idx = int(k + step)

        if end_idx > length:
            end_idx = length

        x_s = x[k:end_idx]
        y_s = y[k:end_idx]
        # print "k: ", k, " end_idx: ", end_idx, "len x_s:", len(x_s), " len y_s: ", len(y_s)

        m, b, r = regression(x_s, y_s)

        m_list.append(m)
        cnt = cnt + 1
    s = np.std(m_list[2:cnt-3])
    q = s/nump
    # raw_input()
    return q


# Argumentos Opcionales
# 'ShowGraph'     : Muestra las grÃ¡ficas del resultado. por defecto no
#                   muestra las grÃ¡ficas
# 'GraphTitle'    : TÃ­tulo a mostrar en la grÃ¡fica.
# 'Error'         : Error mÃ­nimo permitido. es un criterio de parada. Por
#                   defecto 0.001
# 'SmoothFactor'  : Factor de suavisado, argumento de entrada de la
#                   funciÃ³n smooth de matlab. Por defecto 0.07.
# 'SlopeThold'    : MÃ­nimo valor para el factor de pendiente, donde se
#                   concidera que no se debe mover la parte catÃ³dica.
#                   cuando el cambio en la pendiente es mÃ­nimo se asume
#                   pendiente constante. Por defecto 0.15.
# 'SlopeNp'       : NÃºmero de puntos para calcular el factor de
#                   pendiente. Por defecto 10.
# 'NumOfRegions'  : NÃºmero de regiones en las que el algoritmo divide
#                   la data de cada rama. Por defecto 3 regiones.
# 'InitialRegionC': NÃºmero de la region que se concidera como inicial.
#                   esta regiÃ³n serÃ¡ utilizada para calcular
#                   la primera pendiente catÃ³dica. Por defecto 2.
# 'InitialRegionA': NÃºmero de la region que se concidera como inicial.
#                   esta regiÃ³n serÃ¡ utilizada para calcular
#                   la primera pendiente anÃ³dica. Por defecto 2.
# 'MaxIter'       : NÃºmero mÃ¡ximo de iteraciones. TODO: Por definir
# 'MaxIterRegLinC': NÃºmero mÃ¡ximo de iteraciones para el ajuste de la
#                   regresiÃ³n lineal. Por defecto
#                   (length(cathodic_win_potential)/3). es decir 1/3 de la
#                   ventana de datos que se va a analizar.
# 'MaxIterRegLinA': NÃºmero mÃ¡ximo de iteraciones para el ajuste de la
#                   regresiÃ³n lineal. Por defecto
#                   (length(anodic_win_potential)/3). es decir 1/3 de la
#                   ventana de datos que se va a analizar.
# 'LinearAdjThold': Objetivo de Ajuste Lineal, es el set point con el
#                   que se compara el R cuadrado. Si los datos lo permiten
#                   se busca una zona de mejor ajuste para la recta,
#                   reduciendo el tamaÃ±o de la ventana de datos. Valor en
#                   porcentaje, por defecto 95 porciento.
#
# Argumentos Obligatorios
# potential       : Valores de potencial en Voltios
# current         : Valores de corriente en Amperios
#
# Variables de Salida, resultados del anÃ¡lisis
# beta_an         : Beta anÃ³dica en [V/dec]
# beta_ca         : Beta catÃ³dica en [V/dec]
# beta            : Beta calculada como
#                   (beta_an*beta_ca)/2.303*(beta_an+beta_ca) en [V/dec]
# r2_c            : R cuadrado de la regresiÃ³n lineal CatÃ³dica.
# r2_a            : R cuadrado de la regresiÃ³n lineal AnÃ³dica.
# Ecorr           : Potencial de corrosiÃ³n, encontrado como el potenacial
#                   en el cual la corriente es mÃ­nima.
# Icorr           : Valor de la corriente en el punto de corte entre
#                   las rectas.
#
# Variables de Salida, estado del algoritmo
# conv            : 1: si el algoritmo converge, 0: si no converge.
# error_current   : diferencia entre la corriente en los puntos de cruce
#                   de las rectas anÃ³dica y catÃ³dica con la recta vertical Ecorr
# q_slope_anodic  : valor que representa el cambio de pendientes en la rama
#                   anÃ³dica
# q_slope_cathodic: valor que representa el cambio de pendientes en la rama
#                   catÃ³dica
# NoGoRegion      : Region antes y despÃºes de Ecorr que no se concidera
#                   para el calculo de las pendientes.
# MinLinearData   : MÃ­nimo numero de datos para calcular la pendiente.
#                   Es un criterio de parada si llega a esta cantidad
#                   de datos el algoritmo sale con mensaje de no
#                   converge
#

class Tafel:
    def __init__(self, current, potential,
                 Error=0.001,
                 SmoothFactor=0.07,
                 SlopeThold=0.15,
                 SlopeNp=10,
                 NumOfRegions=3,
                 InitialRegionC=2,
                 InitialRegionA=2,
                 MaxIterRegLinC=0,
                 MaxIterRegLinA=0,
                 LinearAdjThold=90,
                 MinLinearData=10,
                 NoGoRegVal=0.01):

        # Algorithm Settings
        self.Error = Error
        self.SmoothFactor = SmoothFactor
        self.SlopeThold = SlopeThold
        self.SlopeNp = SlopeNp
        self.NumOfRegions = NumOfRegions
        self.InitialRegionC = InitialRegionC
        self.InitialRegionA = InitialRegionA
        self.MaxIterRegLinC = MaxIterRegLinC
        self.MaxIterRegLinA = MaxIterRegLinA
        self.LinearAdjThold = LinearAdjThold
        self.MinLinearData = MinLinearData
        self.NoGoRegVal = NoGoRegVal

        # Data
        # self.number = np.array(data[0])
        # self.time = np.array(data[1])
        self.potential = np.array(potential)
        self.current = np.array(current)

        # Resultados

    def smooth(self):
        self.sm_potential = self.potential
        self.sm_current = self.current
        return 0

    def trimInfValues(self):
        for i in range(len(self.potential)):
            if np.isinf(self.potential[i]):
                if i == 0:
                    self.potential[i] = self.potential[i+1]
                else:
                    self.potential[i] = self.potential[i-1]+self.potential[i+1]
            if np.isinf(self.current[i]) or self.current[i] == 0:
                if i == 0:
                    self.current[i] = self.current[i+1]
                else:
                    self.current[i] = self.current[i-1] + self.current[i+1]

    def computeTafelSlopes(self):

        error_current = 1000		# separacion en el eje vertical
        epsilon = self.Error  # error permitido
        step = 1			# direcciÃ³n del movimiento
        error_dir_counter = 1			# contador de cruces por Ecorr. Criterio de parada
        conv = 1			# valor de salida que indica si el algorimo converge.

        # Suvizar la curva
        self.smooth()

        # Eliminar valires infinitos
        self.trimInfValues()

        # Encontrar Ecorr
        abs_current = abs(self.current)
        Ecorr_idx,  = np.where(abs_current == abs_current.min())
        Ecorr_idx = Ecorr_idx[0]

        Ecorr = self.potential[Ecorr_idx]

        # print self.current
        # print abs_current
        # print Ecorr_idx
        # print Ecorr

        log10_current = np.log10(abs(self.sm_current))
        log10_current_nosm = np.log10(abs_current)

        # Encontrar los cuatro puntos iniciales
        cathodic_branch_potential = self.sm_potential[0:Ecorr_idx]
        anodic_branch_potential = self.sm_potential[Ecorr_idx:len(self.sm_potential)]
        # print cathodic_branch_potential
        # print anodic_branch_potential

        cathodic_branch_current = log10_current[0:Ecorr_idx]
        anodic_branch_current = log10_current[Ecorr_idx:len(self.sm_potential)]
        # print cathodic_branch_current
        # print anodic_branch_current

        # Step - Numero de regiones
        cathodic_step = np.floor(
            len(cathodic_branch_potential)/self.NumOfRegions)
        anodic_step = np.floor(
            len(anodic_branch_potential) / self.NumOfRegions)

        # Criterio de +/-50mV
        MinAnodicRegion = Ecorr + self.NoGoRegVal
        MaxCathodicRegion = Ecorr - self.NoGoRegVal

        MinAnodicRegion_idx,   = np.where(
            anodic_branch_potential > MinAnodicRegion)
        MaxCathodicRegion_idx, = np.where(
            cathodic_branch_potential > MaxCathodicRegion)
        MinAnodicRegion_idx = MinAnodicRegion_idx[0]
        MaxCathodicRegion_idx = MaxCathodicRegion_idx[0]

        # print "Min anodic idx : ", MinAnodicRegion_idx
        # print "Max catodic idx: ", MaxCathodicRegion_idx

        # Criterio de +/-20mV
        MinAnodic50mv = Ecorr + 0.05
        MaxCathodic50mv = Ecorr - 0.05
        MinAnodic50mv_idx,   = np.where(
            anodic_branch_potential > MinAnodic50mv)
        MaxCathodic50mv_idx, = np.where(
            cathodic_branch_potential > MaxCathodic50mv)
        MinAnodic50mv_idx = MinAnodic50mv_idx[0]
        MaxCathodic50mv_idx = MaxCathodic50mv_idx[0]

        # idexes de inicio y final de la ventana de trabajo
        cathodic_end_idx = MaxCathodic50mv_idx
        cathodic_start_idx = cathodic_end_idx-cathodic_step
        anodic_end_idx = self.InitialRegionA*anodic_step
        anodic_start_idx = MinAnodic50mv_idx

        # Identificar si las pendientes cambian significativamente
        q_slope_criteria = self.SlopeThold
        q_slope_anodic = slopeChange(
            anodic_branch_potential, anodic_branch_current, self.SlopeNp)
        q_slope_cathodic = slopeChange(
            cathodic_branch_potential, cathodic_branch_current, self.SlopeNp)

        # Critero de ajuste de las pendientes en la regresiÃ³n linear en
        # porcentaje [%]
        linear_adjustment_criteria = self.LinearAdjThold

        # MAIN LOOP - Search algorithm

        while error_current > epsilon:
            prev_err_current = error_current

            # SelecciÃ³n de datos para calcular las pendientes
            cathodic_win_potential = cathodic_branch_potential[int(
                cathodic_start_idx):int(cathodic_end_idx)]
            cathodic_win_current = cathodic_branch_current[int(
                cathodic_start_idx):int(cathodic_end_idx)]

            anodic_win_potential = anodic_branch_potential[int(
                anodic_start_idx):int(anodic_end_idx)]
            anodic_win_current = anodic_branch_current[int(
                anodic_start_idx):int(anodic_end_idx)]

            if self.MaxIterRegLinC == -1:
                self.MaxIterRegLinC = np.floor(len(cathodic_win_potential)/3)

            if self.MaxIterRegLinA == -1:
                self.MaxIterRegLinA = np.floor(len(anodic_win_current)/3)

            # ****** Calcular las pendientes ******

            # Pendiente CatÃ³dica
            if (q_slope_cathodic > q_slope_criteria) and (self.MaxIterRegLinC != 0):
                for k in range(int(self.MaxIterRegLinC)):
                    print("max iter linear : ", int(self.MaxIterRegLinC))
                    mc, bc, rc = regression(
                        cathodic_win_potential, cathodic_win_current)
                    r2c = rc*rc*100
                    if r2c > linear_adjustment_criteria:
                        break
                    else:
                        cathodic_end_idx = cathodic_end_idx - 1
                        cathodic_win_potential = cathodic_branch_potential[
                            cathodic_start_idx:cathodic_end_idx]
                        cathodic_win_current = cathodic_branch_current[cathodic_start_idx:cathodic_end_idx]
            else:
                mc, bc, rc = regression(
                    cathodic_win_potential, cathodic_win_current)

            # Pendiente AnÃ³dica
            if (q_slope_anodic > q_slope_criteria) and (self.MaxIterRegLinA != 0):
                for k in range(int(self.MaxIterRegLinA)):
                    ma, ba, ra = regression(
                        anodic_win_potential, anodic_win_current)
                    r2a = ra*ra*100
                    if r2a > linear_adjustment_criteria:
                        break
                    else:
                        anodic_end_idx = anodic_end_idx-1
                        anodic_win_potential = anodic_branch_potential[anodic_start_idx:anodic_end_idx]
                        anodic_win_current = anodic_branch_current[anodic_start_idx:anodic_end_idx]
            else:
                ma, ba, ra = regression(
                    anodic_win_potential, anodic_win_current)

            # Calcular error respecto a Ecorr
            # recta catodica evaluada en Ecorr
            catodic_ecorr = evaluate_linear(mc, bc, Ecorr)
            # recta anodica evaluada en Ecorr
            anodic_ecorr = evaluate_linear(ma, ba, Ecorr)

            error_current = abs(anodic_ecorr-catodic_ecorr)
            error_potential = abs(Ecorr - (bc-ba)/(ma-mc))

            # Criterio adicional de parada
            if error_current >= prev_err_current:
                if error_dir_counter < 4:
                    error_dir_counter = error_dir_counter + 1
                elif (error_dir_counter > 4) and (error_potential < epsilon):
                    print('Salida por contador de cruces por Ecorr.')
                    break
                else:
                    # recortar la cantidad de datos a analizar
                    error_dir_counter = 0
                    # catodico
                    if (cathodic_end_idx - cathodic_start_idx) < self.MinLinearData:
                        print('WARNING: La funcion no convergio: Minimos datos en la rama catodica...')
                        print('error potencial: {:10.4}'.format(error_potential))
                        print('error corriente: {:10.4}'.format(error_current))
                        conv = 0
                        break
                    else:
                        cathodic_start_idx = cathodic_start_idx + 1

                    # anodico
                    if (anodic_end_idx - anodic_start_idx) < self.MinLinearData:
                        print('WARNING: La funcion no convergio: Minimos datos en la rama anodica...')
                        print('error potencial: {:10.4}'.format(error_potential))
                        print('error corriente: {:10.4}'.format(error_current))
                        conv = 0
                        break
                    else:
                        anodic_end_idx = anodic_end_idx - 1
                        anodic_start_idx = anodic_start_idx + 1

            # Comparar errores
            if anodic_ecorr > catodic_ecorr:
                if (q_slope_anodic > q_slope_criteria):
                    if (anodic_start_idx-step) > MinAnodicRegion_idx:
                        anodic_start_idx = anodic_start_idx-step

                    if (anodic_end_idx-anodic_start_idx) > ((self.MinLinearData*step)+1):
                        anodic_end_idx = anodic_end_idx-step

            elif anodic_ecorr < catodic_ecorr:
                if (q_slope_anodic > q_slope_criteria):
                    if (anodic_end_idx+step) < len(anodic_branch_potential):
                        anodic_end_idx = anodic_end_idx+step

                    if (anodic_end_idx-anodic_start_idx) > ((self.MinLinearData*step)+1):
                        anodic_start_idx = anodic_start_idx+step
            else:
                print('Eureka!')
                break

        # RESULTADOS
        print("RESULTADOS")

        # calculo de betas
        beta_ca = 1/mc
        beta_an = 1/ma
        beta_c_mVpDec = beta_ca*1000
        beta_a_mVpDec = beta_an*1000
        beta = (abs(beta_c_mVpDec)*abs(beta_a_mVpDec)) / \
            (2.303*(abs(beta_c_mVpDec)+abs(beta_a_mVpDec)))

        if abs(beta_c_mVpDec) > 600:
            # beta catodica se asume infinita y se aplica aproximaciÃ³n
            beta = abs(beta_a_mVpDec)/2.303
            print('WARNING: Se asume (Bc+Ba) aproximadamente igual a Bc. Por tanto B=Ba/2.303')

        # Estimado de Icorr
        Icorr1 = evaluate_linear(mc, bc, Ecorr)
        Icorr2 = evaluate_linear(ma, ba, Ecorr)
        Icorr = (Icorr1+Icorr2)/2

        # Calculo de R cuadrado
        r2_a = ra*ra
        r2_c = rc*rc

        # Mostrar resultados en Terminal
        print('\nResultados TAFEL ')

        print('Beta Catodica: {:+010.4} [mV/dec]'.format(beta_c_mVpDec))
        print('Beta Anodica : {:+010.4} [mV/dec]'.format(beta_a_mVpDec))
        print('Beta         : {:+010.4} [mV/dec]'.format(beta))

        print('Factor de ajuste catodico: {:10.3g} [%]'.format(r2_c*100))
        print('Factor de ajuste anodico : {:10.3g} [%]'.format(r2_a*100))

        print('Ecorr : {:10.4} [mV]'.format(Ecorr*1000))
        print('Icorr : {:10.4} [uA] (Valor Estimado)'.format((10**Icorr)*1000000))

        # print "*******************************************"
        # print "Ecorr Anodico   : ", anodic_ecorr
        # print "Ecorr Catodico  : ", catodic_ecorr
        # print "Error Corriente : ", error_current

        # Varibles para la grafica
        self.log10_current = np.log10(abs(self.sm_current*1000000))
        self.cathodic_win_potential = cathodic_win_potential
        self.cathodic_win_current = cathodic_win_current
        self.anodic_win_potential = anodic_win_potential
        self.anodic_win_current = anodic_win_current

        self.cathodic_end_idx = cathodic_end_idx
        self.cathodic_start_idx = cathodic_start_idx
        self.anodic_end_idx = anodic_end_idx
        self.anodic_start_idx = anodic_start_idx

        self.abscisa_anodic = self.sm_potential[int(
            cathodic_end_idx)+10:int(Ecorr_idx+anodic_end_idx)]
        self.abscisa_cathodic = self.sm_potential[int(
            cathodic_start_idx):int(Ecorr_idx+MinAnodic50mv_idx)]
        self.y_anodic = evaluate_linear(ma, ba, self.abscisa_anodic)
        self.y_cathodic = evaluate_linear(mc, bc, self.abscisa_cathodic)
        self.log10_current_nosm = log10_current_nosm

        self.Ecorr = Ecorr
        self.Ecorr_idx = Ecorr_idx
        self.beta_an = beta_an
        self.beta_ca = beta_ca
        self.beta = beta
        self.MinAnodicRegion_idx = MinAnodicRegion_idx
        self.MaxCathodicRegion_idx = MaxCathodicRegion_idx
        self.beta_c_mVpDec = beta_c_mVpDec
        self.beta_a_mVpDec = beta_a_mVpDec
        self.icorr = 10**Icorr
