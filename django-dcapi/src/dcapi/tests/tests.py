from django.test import TestCase
import json
from pathlib import Path
import numpy as np

from dcapi.tools import computePolRes, Tafel
# Create your tests here.


class LprTestCase(TestCase):

    def test_lpr(self):
        f = open(str(Path(__file__).absolute()).replace(
            "tests.py", "lpr1.json"))
        lpr1_data = json.load(f)
        res = computePolRes(lpr1_data['current'], lpr1_data['potential'])
        print(json.dumps(res, indent=2))


class TafelTestCase(TestCase):
    def test_tafel(self):
        f = open(str(Path(__file__).absolute()).replace(
            "tests.py", "tafel2.json"))
        tafel1_data = json.load(f)

        potential = np.array(tafel1_data['potential'])
        current = np.array(tafel1_data['current'])

        if potential[0] > potential[-1]:
            potential = np.flip(potential)
            current = np.flip(current)


        tfl = Tafel(current, potential)
        tfl.computeTafelSlopes()
        # print(json.dumps({
        #     'potential': list(tfl.potential),
        #     'current': list(tfl.current),
        #     'potential_cathodic_points': list(tfl.potential[int(tfl.cathodic_start_idx):int(tfl.cathodic_end_idx)]),
        #     'current_cathodic_points': list(tfl.current[int(tfl.cathodic_start_idx):int(tfl.cathodic_end_idx)]),
        #     'potential_anodic_points': list(tfl.potential[int(tfl.Ecorr_idx + tfl.anodic_start_idx):int(tfl.Ecorr_idx + tfl.anodic_end_idx)]),
        #     'current_anodic_points': list(tfl.current[int(tfl.Ecorr_idx + tfl.anodic_start_idx):int(tfl.Ecorr_idx + tfl.anodic_end_idx)]),
        #     'linear_potential_cathodic': list(tfl.abscisa_cathodic),
        #     'linear_potential_anodic': list(tfl.abscisa_anodic),
        #     'linear_current_cathodic': list(tfl.y_cathodic),
        #     'linear_current_anodic': list(tfl.y_anodic),
        #     'beta_cathodic': tfl.beta_ca,
        #     'beta_anodic': tfl.beta_an,
        #     'beta': tfl.beta,
        #     'Ecorr': tfl.Ecorr,
        #     'icorr': tfl.icorr,
        # }, indent=2))
