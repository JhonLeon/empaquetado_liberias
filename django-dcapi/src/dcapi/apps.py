from django.apps import AppConfig


class DcapiConfig(AppConfig):
    name = 'dcapi'
