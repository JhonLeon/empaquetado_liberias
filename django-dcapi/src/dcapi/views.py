from rest_framework.decorators import api_view
from rest_framework.response import Response

import numpy as np

from .tools import computePolRes, Tafel


@api_view()
def hello_world(request):
    return Response({"message": "Hello, world!"})


@api_view(['POST'])
def lpr(request):
    potential = np.array(request.data.get('potential', []))
    current = np.array(request.data.get('current', []))
    max_iters = request.data.get('max_iter', 1000)
    linear_adjustment_criteria = request.data.get(
        'linear_adjustment_criteria', 99)
    min_number_of_points = request.data.get('min_number_of_points', 6)

    result = computePolRes(current, potential, max_iters,
                           linear_adjustment_criteria, min_number_of_points)

    return Response(result)


@api_view(['POST'])
def tafel(request):
    potential = np.array(request.data.get('potential', []))
    current = np.array(request.data.get('current', []))

    if potential[0] > potential[-1]:
        potential = np.flip(potential)
        current = np.flip(current)

    tfl = Tafel(current, potential,
                Error=0.001,
                SmoothFactor=0.07,
                SlopeThold=0.15,
                SlopeNp=10,
                NumOfRegions=3,
                InitialRegionC=2,
                InitialRegionA=2,
                MaxIterRegLinC=0,
                MaxIterRegLinA=0,
                LinearAdjThold=90,
                MinLinearData=10,
                NoGoRegVal=0.01)

    tfl.computeTafelSlopes()

    result = {
        'potential': list(tfl.potential),
        'current': list(tfl.current),
        'potential_cathodic_points': list(tfl.potential[int(tfl.cathodic_start_idx):int(tfl.cathodic_end_idx)]),
        'current_cathodic_points': list(tfl.current[int(tfl.cathodic_start_idx):int(tfl.cathodic_end_idx)]),
        'potential_anodic_points': list(tfl.potential[int(tfl.Ecorr_idx + tfl.anodic_start_idx):int(tfl.Ecorr_idx + tfl.anodic_end_idx)]),
        'current_anodic_points': list(tfl.current[int(tfl.Ecorr_idx + tfl.anodic_start_idx):int(tfl.Ecorr_idx + tfl.anodic_end_idx)]),
        'linear_potential_cathodic': list(tfl.abscisa_cathodic),
        'linear_potential_anodic': list(tfl.abscisa_anodic),
        'linear_current_cathodic': list(tfl.y_cathodic),
        'linear_current_anodic': list(tfl.y_anodic),
        'beta_cathodic': tfl.beta_ca,
        'beta_anodic': tfl.beta_an,
        'beta': tfl.beta,
        'Ecorr': tfl.Ecorr,
        'icorr': tfl.icorr,
    }

    return Response(result)
