¡¡¡¡¡¡¡¡¡¡¡¡Paso para actualizar y empaquetar libreria!!!!!!!!!!!!!!

1 - Activar el entorno virtual de forma global en la carpeta BACKEND_MULTICIC
2 - Realizar las modificaciones que desee dentre de cada libreria en el directorio django-<nombreLibreria>/src/<nombreLibreria>
3 - Eliminar el directorio (dist) de la libreria en actualización
4 - En el archivo setup.cfg cambiar la versión con el siguiente formato "version = #.#.#"
5 - Ejecutar el comando < python -m build >
6 - Cerificar que el directorio dist se haya creado con los siguientes archivos (django_<nombreLibreria>-#.#.#-py3-none-any.whl) y (django-<nombreLibreria>-#.#.#.tar.gz)
7 - Actualizar repositorio.
8 - Crear en el directorio .pypirc con el siguiente contenido:

            [distutils]
            index-servers =
                gitlab

            [gitlab]
            repository = https://gitlab.com/api/v4/projects/24072066/packages/pypi
            username = __Nombre_token__
            password = __token__

    El reposirory apunta al reposytorio donde se desea almacenar la libreria en gitlab en Package Registry (Modificar si es preciso)

9 - Ejecutar el siguiente comando < python -m twine upload --repository gitlab dist/* >
10 - Verificar si la actualización de la libreria fue exitosa.

